package com.secata.stream;

import java.util.concurrent.atomic.AtomicBoolean;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public class DataStreamSerializableTest {

  @Test
  public void accept() {
    AtomicBoolean test = new AtomicBoolean(false);
    DataStreamSerializable dataStreamSerializable =
        stream -> {
          Assertions.assertThat(stream).isNull();
          test.set(true);
        };
    dataStreamSerializable.accept(null);
    Assertions.assertThat(test).isTrue();
  }
}
