package com.secata.stream;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SafeDataOutputStreamTest {

  private SafeDataOutputStream workingStream;
  private SafeDataOutputStream brokenStream;
  private ByteArrayOutputStream stream;
  private byte[] referenceData;

  /** Setup. */
  @BeforeEach
  public void setUp() {
    referenceData = new byte[] {0, 1, 2, 3, 4, 5, 6, 7};
    stream = new ByteArrayOutputStream();
    workingStream = new SafeDataOutputStream(stream);
    brokenStream = new SafeDataOutputStream(null);
  }

  @Test
  public void serialize() {
    byte[] serialize = SafeDataOutputStream.serialize(stream -> stream.write(referenceData));
    Assertions.assertThat(serialize).containsExactly(referenceData);
  }

  private void runTest(Consumer<SafeDataOutputStream> consumer) {
    consumer.accept(workingStream);
    Assertions.assertThat(referenceData).startsWith(stream.toByteArray());

    Assertions.assertThatThrownBy(() -> consumer.accept(brokenStream))
        .hasMessageContaining("Unable to write")
        .isExactlyInstanceOf(RuntimeException.class);
  }

  @Test
  public void size() {
    ByteArrayOutputStream inner = new ByteArrayOutputStream();
    SafeDataOutputStream stream = new SafeDataOutputStream(inner);
    stream.writeLong(1L);
    Assertions.assertThat(inner.size()).isEqualTo(8);
    Assertions.assertThat(stream.size()).isEqualTo(8);
  }

  @Test
  public void writeLong() {
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeLong(283686952306183L));
  }

  @Test
  public void writeInt() {
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeInt(66051));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeInt(67438087));
  }

  @Test
  public void writeIntAsShort() {
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort(1));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort(515));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort(1029));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort(1543));
  }

  @Test
  public void writeShort() {
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort((short) 1));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort((short) 515));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort((short) 1029));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeShort((short) 1543));
  }

  @Test
  public void writeSignedByte() {
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(0));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(1));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(2));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(3));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(4));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(5));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(6));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeByte(7));
  }

  @Test
  public void writeFully() {
    runTest(stream -> stream.write(referenceData));
  }

  @Test
  public void writeDynamicBytes() {
    referenceData = new byte[] {0, 0, 0, 8, 0, 1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0};
    runTest(stream -> stream.writeDynamicBytes(new byte[] {0, 1, 2, 3, 4, 5, 6, 7}));
    runTest(stream -> stream.writeDynamicBytes(new byte[0]));
  }

  @Test
  public void writeBoolean() {
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeBoolean(false));
    runTest((safeDataOutputStream) -> safeDataOutputStream.writeBoolean(true));
  }

  @Test
  public void writeString() {
    referenceData = new byte[] {0, 0, 0, 6, 65, 66, 67, 32, 66, 66};
    runTest(s -> s.writeString("ABC BB"));
    Assertions.assertThat(referenceData).containsExactly(stream.toByteArray());
  }

  @Test
  public void writeFixedList() {
    List<Byte> expected = new ArrayList<>();
    for (byte value : referenceData) {
      expected.add(value);
    }
    SafeListStream<Byte> listStream =
        SafeListStream.create(
            SafeDataInputStream::readSignedByte,
            SafeListStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte));
    runTest(s -> listStream.writeFixed(s, expected));
  }

  @Test
  public void writeDynamicList() {
    List<Byte> expected = new ArrayList<>();
    for (byte value : referenceData) {
      expected.add(value);
    }
    SafeListStream<Byte> listStream =
        SafeListStream.create(
            SafeDataInputStream::readSignedByte,
            SafeCollectionStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte));
    referenceData = new byte[] {0, 0, 0, 8, 0, 1, 2, 3, 4, 5, 6, 7};
    listStream.writeDynamic(workingStream, expected);
    Assertions.assertThat(stream.toByteArray()).containsExactly(referenceData);
  }

  @Test
  public void writeEnum() {
    runTest(s -> s.writeEnum(TestEnum.A));
    runTest(s -> s.writeEnum(TestEnum.B));
    runTest(s -> s.writeEnum(TestEnum.C));
    runTest(s -> s.writeEnum(TestEnum.D));
    runTest(s -> s.writeEnum(TestEnum.E));
    runTest(s -> s.writeEnum(TestEnum.F));
    runTest(s -> s.writeEnum(TestEnum.G));
    runTest(s -> s.writeEnum(TestEnum.H));
  }

  @Test
  public void writeOptional() {
    referenceData = new byte[] {0, 1, 0, 0, 0, 1, 1, 127, -1, -1, -1};

    runTest(s -> s.writeOptional(SafeListStream.primitive(SafeDataOutputStream::writeInt), null));
    runTest(s -> s.writeOptional(SafeListStream.primitive(SafeDataOutputStream::writeInt), 1));
    runTest(
        s ->
            s.writeOptional(
                SafeListStream.primitive(SafeDataOutputStream::writeInt), Integer.MAX_VALUE));
  }

  @Test
  public void writeOptionalList() {
    referenceData = new byte[] {0, 1, 0, 0, 0, 1, 1, 127, -1, -1, -1};
    ArrayList<Integer> arrayList = new ArrayList<>();
    arrayList.add(null);
    arrayList.add(1);
    arrayList.add(Integer.MAX_VALUE);

    SafeListStream<Integer> listStream =
        SafeListStream.createOptional(
            SafeDataInputStream::readInt, SafeListStream.primitive(SafeDataOutputStream::writeInt));
    runTest(s -> listStream.writeFixed(s, arrayList));
  }

  @Test
  public void writeUuid() {
    UUID uuid = new UUID(0x7FFFFFFFFFFFFFFFL, 0x6FFEEEEEEEEEEEEEL);
    referenceData =
        new byte[] {
          127, -1, -1, -1, -1, -1, -1, -1,
          111, -2, -18, -18, -18, -18, -18, -18
        };

    runTest(s -> s.writeUuid(uuid));

    byte[] bytes = stream.toByteArray();
    Assertions.assertThat(bytes).containsExactly(referenceData);
  }
}
