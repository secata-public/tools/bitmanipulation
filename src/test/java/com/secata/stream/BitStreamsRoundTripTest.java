package com.secata.stream;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Function;
import org.junit.jupiter.api.Test;

/** Test of {@link BitInput} and {@link BitOutput}. */
public final class BitStreamsRoundTripTest {

  /**
   * Utility asserter testing that all the given examples can be serialized using {@code writer} and
   * then deserialized using {@code reader} without losing any information.
   *
   * @param <T> Type of elements.
   * @param examples Examples to write and read.
   * @param writer Writing method for T.
   * @param reader Reading method for T.
   * @return Returns the length of the serialized array.
   */
  <T> int assertCanReadWhatIsWritten(
      final Iterable<T> examples,
      final BiConsumer<BitOutput, T> writer,
      final Function<BitInput, T> reader) {
    final var outputBytes = new ByteArrayOutputStream();

    // Write
    final BitOutput output = new BitOutput(outputBytes);
    for (final T input : examples) {
      writer.accept(output, input);
    }

    output.flushLastByte();

    // Read
    final byte[] serializedBytes = outputBytes.toByteArray();
    final BitInput input = BitInput.create(new ByteArrayInputStream(serializedBytes));
    final List<T> read = new ArrayList<>();
    for (final T unused : examples) {
      read.add(reader.apply(input));
    }

    // Check
    assertThat(read)
        .as("Can read what was written.")
        .usingRecursiveComparison()
        .isEqualTo(examples);

    return serializedBytes.length;
  }

  /** Random generator for use in {@link assertCanWriteWhatIsRead}. */
  private final Random rng = new SecureRandom();

  /**
   * Utility asserter testing that whatever is deserialized by {@code reader} can be re-serialized
   * by {@code writer} to the same byte contents.
   *
   * @param <T> Type of elements.
   * @param numElements Number of elements to read.
   * @param elementSizeBytes Size of each element in bytes.
   * @param writer Writing method for T.
   * @param reader Reading method for T.
   */
  <T> void assertCanWriteWhatIsRead(
      final int numElements,
      final int elementSizeBytes,
      final BiConsumer<BitOutput, T> writer,
      final Function<BitInput, T> reader) {

    // Generate bytes
    final byte[] inputBytes = new byte[numElements * elementSizeBytes];
    rng.nextBytes(inputBytes);

    // Read
    final BitInput input = BitInput.create(new ByteArrayInputStream(inputBytes));
    final List<T> read = new ArrayList<>();
    for (int idx = 0; idx < numElements; idx++) {
      read.add(reader.apply(input));
    }

    // Write
    final var outputBytes = new ByteArrayOutputStream();
    final BitOutput output = new BitOutput(outputBytes);
    for (final T element : read) {
      writer.accept(output, element);
    }

    // Check
    assertThat(outputBytes.toByteArray())
        .as("Can write what is read: " + read)
        .isEqualTo(inputBytes);
  }

  /**
   * Convenience asserter combining {@link assertCanReadWhatIsWritten} and {@link
   * assertCanWriteWhatIsRead}.
   *
   * @param <T> Type of elements.
   * @param examples Examples for {@link assertCanReadWhatIsWritten}.
   * @param writer Writing method for T.
   * @param reader Reading method for T.
   */
  <T> void assertCanReadReadWriteWrite(
      final Collection<T> examples,
      final BiConsumer<BitOutput, T> writer,
      final Function<BitInput, T> reader) {
    final int totalBytesWritten = assertCanReadWhatIsWritten(examples, writer, reader);
    final int numElements = 100;
    final int elementSizeBytes = totalBytesWritten / examples.size();
    assertCanWriteWhatIsRead(numElements, elementSizeBytes, writer, reader);
  }

  /** {@link Boolean}s can be serialized and then deserialized to the same value. */
  @Test
  void readWriteBoolean() {
    assertCanReadWhatIsWritten(
        ExampleObjects.BOOLEANS, BitOutput::writeBoolean, BitInput::readBoolean);
  }

  /** {@link Integer}s can be serialized and then deserialized to the same value. */
  @Test
  void readWriteIntegers() {
    assertCanReadReadWriteWrite(
        ExampleObjects.BYTES, (s, v) -> s.writeSignedInt(v, 8), s -> (byte) s.readSignedInt(8));
    assertCanReadReadWriteWrite(
        ExampleObjects.SHORTS, (s, v) -> s.writeSignedInt(v, 16), s -> (short) s.readSignedInt(16));
    assertCanReadReadWriteWrite(
        ExampleObjects.INTEGERS, (s, v) -> s.writeSignedInt(v, 32), s -> s.readSignedInt(32));
    assertCanReadReadWriteWrite(
        ExampleObjects.LONGS, (s, v) -> s.writeSignedLong(v, 64), s -> s.readSignedLong(64));
  }

  /** {@link BigInteger}s can be serialized and then deserialized to the same value. */
  @Test
  void readWriteBigIntegers() {
    assertCanReadReadWriteWrite(
        ExampleObjects.BIG_INTEGERS_16_POSITIVE,
        (s, v) -> s.writeSignedBigInteger(v, 128),
        s -> s.readSignedBigInteger(128));
    assertCanReadReadWriteWrite(
        ExampleObjects.BIG_INTEGERS_16_UNSIGNED,
        (s, v) -> s.writeUnsignedBigInteger(v, 128),
        s -> s.readUnsignedBigInteger(128));
  }
}
