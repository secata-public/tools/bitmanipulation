package com.secata.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SafeDataInputStreamTest {

  private byte[] bytes;
  private SafeDataInputStream workingStream;

  /** Setup. */
  @BeforeEach
  public void setUp() {
    bytes = new byte[] {0, 1, 2, 3, 4, 5, 6, 7};
    workingStream = data(bytes);
  }

  private SafeDataInputStream data(byte[] bytes) {
    return SafeDataInputStream.createFromBytes(bytes);
  }

  private <T> void runTest(T expected, Function<SafeDataInputStream, T> function) {
    Assertions.assertThat(function.apply(workingStream)).isEqualTo(expected);
    Assertions.assertThatThrownBy(() -> SafeDataInputStream.deserialize(function, new byte[0]))
        .hasMessageContaining("Unable to read")
        .isExactlyInstanceOf(RuntimeException.class);
  }

  @Test
  public void readLong() {
    Assertions.assertThat(SafeDataInputStream.deserialize(SafeDataInputStream::readLong, bytes))
        .isEqualTo(283686952306183L);
    runTest(283686952306183L, SafeDataInputStream::readLong);
  }

  @Test
  public void readInt() {
    Assertions.assertThat(SafeDataInputStream.deserialize(SafeDataInputStream::readInt, bytes))
        .isEqualTo(66051);
    runTest(66051, SafeDataInputStream::readInt);
    runTest(67438087, SafeDataInputStream::readInt);
  }

  @Test
  public void readUnsignedByte() {
    runTest(0, SafeDataInputStream::readUnsignedByte);
    runTest(1, SafeDataInputStream::readUnsignedByte);
    runTest(2, SafeDataInputStream::readUnsignedByte);
    runTest(3, SafeDataInputStream::readUnsignedByte);
    runTest(4, SafeDataInputStream::readUnsignedByte);
    runTest(5, SafeDataInputStream::readUnsignedByte);
    runTest(6, SafeDataInputStream::readUnsignedByte);
    runTest(7, SafeDataInputStream::readUnsignedByte);
  }

  @Test
  public void readSignedByte() {
    runTest((byte) 0, SafeDataInputStream::readSignedByte);
    runTest((byte) 1, SafeDataInputStream::readSignedByte);
    runTest((byte) 2, SafeDataInputStream::readSignedByte);
    runTest((byte) 3, SafeDataInputStream::readSignedByte);
    runTest((byte) 4, SafeDataInputStream::readSignedByte);
    runTest((byte) 5, SafeDataInputStream::readSignedByte);
    runTest((byte) 6, SafeDataInputStream::readSignedByte);
    runTest((byte) 7, SafeDataInputStream::readSignedByte);
  }

  @Test
  public void readSignedShort() {
    bytes[0] = -128;
    workingStream = data(bytes);
    runTest((short) -32767, SafeDataInputStream::readShort);
  }

  @Test
  public void readUnsignedShort() {
    bytes[0] = (byte) 0xFF;
    bytes[1] = (byte) 0xFF;
    workingStream = data(bytes);
    runTest(65535, SafeDataInputStream::readUnsignedShort);
  }

  @Test
  public void readShort() {
    runTest((short) 1, SafeDataInputStream::readShort);
    runTest((short) 515, SafeDataInputStream::readShort);
    runTest((short) 1029, SafeDataInputStream::readShort);
    runTest((short) 1543, SafeDataInputStream::readShort);
  }

  @Test
  public void readBoolean() {
    runTest(false, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
    runTest(true, SafeDataInputStream::readBoolean);
  }

  @Test
  public void readBytes() {
    byte[] bytes = new byte[23];
    bytes[22] = 1;
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(bytes);
    byte[] read = stream.readBytes(23);
    Assertions.assertThat(read).isEqualTo(bytes);

    SafeDataInputStream secondStream = SafeDataInputStream.createFromBytes(bytes);
    Assertions.assertThatThrownBy(() -> secondStream.readBytes(24))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Unable to read requested number of bytes");
  }

  @Test
  public void readAllBytes() {
    byte[] manyBytes = new byte[Short.MAX_VALUE];
    Assertions.assertThat(data(manyBytes).readAllBytes()).isEqualTo(manyBytes);
    Assertions.assertThat(data(new byte[0]).readAllBytes()).isEmpty();
  }

  @Test
  public void readDynamicBytes() {
    workingStream = data(new byte[] {0, 0, 0, 8, 0, 1, 2, 3, 4, 5, 6, 7});
    runTest(this.bytes, SafeDataInputStream::readDynamicBytes);
  }

  @Test
  public void readString() {
    workingStream = data(new byte[] {0, 0, 0, 6, 65, 66, 67, 32, 66, 66});
    runTest("ABC BB", SafeDataInputStream::readString);
  }

  @Test
  public void readFixedList() {
    List<Object> expected = new ArrayList<>();
    for (byte value : bytes) {
      expected.add(value);
    }
    SafeFixedListStream<Byte> listStream =
        SafeFixedListStream.create(
            SafeDataInputStream::readSignedByte,
            SafeCollectionStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte));
    runTest(
        expected,
        safeDataInputStream -> new ArrayList<>(listStream.readFixed(safeDataInputStream, 8)));
  }

  @Test
  public void readFixed() {
    List<Object> expected = new ArrayList<>();
    for (byte value : bytes) {
      expected.add(value);
    }
    SafeListStream<Byte> listStream =
        SafeListStream.create(
            SafeDataInputStream::readSignedByte,
            SafeListStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte));
    runTest(expected, safeDataInputStream -> listStream.readFixed(safeDataInputStream, 8));
  }

  @Test
  public void readDynamic() {
    List<Byte> expected = new ArrayList<>();
    expected.add((byte) 127);
    workingStream = data(new byte[] {0, 0, 0, 1, 127});

    SafeListStream<Byte> listStream =
        SafeListStream.create(
            SafeDataInputStream::readSignedByte,
            SafeCollectionStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte));
    runTest(expected, listStream::readDynamic);
  }

  @Test
  public void readEnum() {
    runTest(TestEnum.A, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.B, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.C, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.D, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.E, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.F, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.G, s -> s.readEnum(TestEnum.values()));
    runTest(TestEnum.H, s -> s.readEnum(TestEnum.values()));
  }

  @Test
  public void readOptional() {
    workingStream = data(new byte[] {0, 1, 0, 0, 0, 1, 1, 127, -1, -1, -1});

    Function<SafeDataInputStream, Integer> readFunction =
        stream -> stream.readOptional(SafeDataInputStream::readInt);
    runTest(null, readFunction);
    runTest(1, readFunction);
    runTest(Integer.MAX_VALUE, readFunction);
  }

  @Test
  public void readOptionalList() {
    workingStream = data(new byte[] {0, 1, 0, 0, 0, 1, 1, 127, -1, -1, -1});

    ArrayList<Integer> arrayList = new ArrayList<>();
    arrayList.add(null);
    arrayList.add(1);
    arrayList.add(Integer.MAX_VALUE);

    SafeListStream<Integer> listStream =
        SafeListStream.createOptional(
            SafeDataInputStream::readInt,
            SafeCollectionStream.primitive(SafeDataOutputStream::writeInt));
    runTest(arrayList, s -> listStream.readFixed(s, 3));
  }

  @Test
  public void consumeFully() {
    byte[] simpleByteArray = new byte[] {1, 2, 3, 4, 5};
    byte[] fully =
        SafeDataInputStream.readFully(
            simpleByteArray,
            stream ->
                new byte[] {
                  stream.readSignedByte(), stream.readSignedByte(),
                  stream.readSignedByte(), stream.readSignedByte(),
                  stream.readSignedByte()
                });
    Assertions.assertThat(fully).containsExactly(1, 2, 3, 4, 5);

    Assertions.assertThatThrownBy(
            () -> SafeDataInputStream.readFully(simpleByteArray, ignored -> null))
        .hasMessageContaining("Entire content of stream was not read")
        .hasMessageContaining("5");
    Assertions.assertThatThrownBy(
            () ->
                SafeDataInputStream.readFully(
                    simpleByteArray,
                    creator -> {
                      creator.readInt();
                      return null;
                    }))
        .hasMessageContaining("Entire content of stream was not read")
        .hasMessageContaining("1");
  }

  @Test
  public void skipBytes() {
    byte[] bytes = new byte[23];
    bytes[22] = 1;
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(bytes);
    stream.skipBytes(22);
    Assertions.assertThat(stream.readUnsignedByte()).isEqualTo(1);

    SafeDataInputStream secondStream = SafeDataInputStream.createFromBytes(bytes);
    Assertions.assertThatThrownBy(() -> secondStream.skipBytes(24))
        .hasMessageContaining("Unable to skip requested number of bytes");
  }

  @Test
  public void readUuid() {
    final UUID uuid = new UUID(0x7FFFFFFFFFFFFFFFL, 0x6FFEEEEEEEEEEEEEL);
    final byte[] bytes =
        new byte[] {
          127, -1, -1, -1, -1, -1, -1, -1,
          111, -2, -18, -18, -18, -18, -18, -18
        };

    SafeDataInputStream in = SafeDataInputStream.createFromBytes(bytes);
    Assertions.assertThat(in.readUuid()).isEqualTo(uuid);
  }
}
