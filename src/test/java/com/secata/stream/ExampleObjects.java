package com.secata.stream;

import java.math.BigInteger;
import java.util.List;

/** Utility class for storing lists of example objects for use in serialization tests. */
public final class ExampleObjects {

  /** Long Examples. */
  public static final List<Long> LONGS =
      List.of(Long.MIN_VALUE, Long.MAX_VALUE, 0x0001020304050607L);

  /** Integer Examples. */
  public static final List<Integer> INTEGERS =
      List.of(Integer.MIN_VALUE, Integer.MAX_VALUE, 0x00010203, 0x04050607);

  /** Short Examples. */
  public static final List<Short> SHORTS =
      List.of(
          Short.MIN_VALUE,
          Short.MAX_VALUE,
          (short) 0x0001,
          (short) 0x0203,
          (short) 0x0405,
          (short) 0x0607);

  /** Byte Examples. */
  public static final List<Byte> BYTES =
      List.of(
          Byte.MIN_VALUE,
          Byte.MAX_VALUE,
          (byte) 0x00,
          (byte) 0x01,
          (byte) 0x02,
          (byte) 0x03,
          (byte) 0x04,
          (byte) 0x05,
          (byte) 0x06,
          (byte) 0x07);

  /** Byte Array Examples. */
  public static final List<byte[]> EIGHT_BYTE_ARRAYS =
      List.of(new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07});

  /** Boolean Examples. */
  public static final List<Boolean> BOOLEANS = List.of(false, true);

  /** Big integer examples. */
  public static final List<BigInteger> BIG_INTEGERS_16_POSITIVE =
      List.of(
          BigInteger.ONE,
          new BigInteger("31"),
          new BigInteger("32"),
          new BigInteger("255"),
          new BigInteger("256"),
          new BigInteger("65535"),
          new BigInteger("65536"),
          new BigInteger("65537"),
          new BigInteger("4294967295"),
          new BigInteger("4294967296"),
          new BigInteger("000102030405060708090A0B0C0D0E0F", 16),
          new BigInteger("18446744073709551615"),
          new BigInteger("18446744073709551616"),
          new BigInteger("18446744073709551617"),
          new BigInteger("85070591730234615865843651857942052863"),
          new BigInteger("85070591730234615865843651857942052864"),
          new BigInteger("85070591730234615865843651857942052865"),
          new BigInteger("170141183460469231731687303715884105726"),
          new BigInteger("170141183460469231731687303715884105727"));

  /** Big integer examples. */
  public static final List<BigInteger> BIG_INTEGERS_16_UNSIGNED =
      List.of(
          new BigInteger("170141183460469231731687303715884105728"),
          new BigInteger("340282366920938463463374607431768211455"));

  /** Big integer examples. */
  public static final List<BigInteger> BIG_INTEGERS_17_UNSIGNED =
      List.of(
          new BigInteger("340282366920938463463374607431768211456"),
          new BigInteger("340282366920938463463374607431768211457"));
}
