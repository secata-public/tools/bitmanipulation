package com.secata.stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Test. */
@SuppressWarnings("CPD-START")
public class BigEndianByteInputTest {
  @Test
  public void readI64() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readI64()).isEqualTo(0x0001020304050607L);
    assertThatThrownBy(workingStream::readI64)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read long");
  }

  @Test
  public void readU64() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readU64()).isEqualTo(0x0001020304050607L);
    assertThatThrownBy(workingStream::readU64)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read long");
  }

  @Test
  public void readI32() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readI32()).isEqualTo(0x00010203);
    assertThat(workingStream.readI32()).isEqualTo(0x04050607);
    assertThatThrownBy(workingStream::readI32)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read int");
  }

  @Test
  public void readU32() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readU32()).isEqualTo(0x00010203);
    assertThat(workingStream.readU32()).isEqualTo(0x04050607);
    assertThatThrownBy(workingStream::readU32)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read int");
  }

  @Test
  public void readI8() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x00);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x01);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x02);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x03);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x04);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x05);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x06);
    assertThat(workingStream.readI8()).isEqualTo((byte) 0x07);
    assertThatThrownBy(workingStream::readI8)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read byte");
  }

  @Test
  public void readU8() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x00);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x01);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x02);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x03);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x04);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x05);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x06);
    assertThat(workingStream.readU8()).isEqualTo((byte) 0x07);
    assertThatThrownBy(workingStream::readU8)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read byte");
  }

  @Test
  public void readI16() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readI16()).isEqualTo((short) 0x0001);
    assertThat(workingStream.readI16()).isEqualTo((short) 0x0203);
    assertThat(workingStream.readI16()).isEqualTo((short) 0x0405);
    assertThat(workingStream.readI16()).isEqualTo((short) 0x0607);
    assertThatThrownBy(workingStream::readI16)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read short");
  }

  @Test
  public void readU16() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readU16()).isEqualTo((short) 0x0001);
    assertThat(workingStream.readU16()).isEqualTo((short) 0x0203);
    assertThat(workingStream.readU16()).isEqualTo((short) 0x0405);
    assertThat(workingStream.readU16()).isEqualTo((short) 0x0607);
    assertThatThrownBy(workingStream::readU16)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read short");
  }

  @Test
  public void readBoolean() {
    var bytes = new byte[] {0x00, 0x01, 0x02};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readBoolean()).isFalse();
    assertThat(workingStream.readBoolean()).isTrue();
    assertThat(workingStream.readBoolean()).isTrue();
    assertThatThrownBy(workingStream::readBoolean)
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read byte");
  }

  @Test
  public void readBytes() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readBytes(8)).isEqualTo(bytes);
    assertThatThrownBy(() -> workingStream.readBytes(8))
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read bytes");
  }

  /** A malformed length should not try to allocate a very large byte array. */
  @Test
  void readBytesShouldNotAllocateLargerArrayThanAvailableInStream() {
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(new byte[1]));
    assertThatThrownBy(() -> workingStream.readBytes(Integer.MAX_VALUE))
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read bytes");
  }

  @Test
  public void readSignedBigInteger() {
    var bytes = new byte[] {-1, -1, -1, -2};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readSignedBigInteger(4)).isEqualTo(new BigInteger("-2"));
    assertThatThrownBy(() -> workingStream.readSignedBigInteger(4))
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read big integer with 4 bytes");
  }

  @Test
  public void readUnsignedBigInteger() {
    var bytes = new byte[] {-1, -1, -1, -2};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThat(workingStream.readUnsignedBigInteger(4)).isEqualTo(new BigInteger("fffffffe", 16));
    assertThatThrownBy(() -> workingStream.readUnsignedBigInteger(4))
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Unable to read big integer with 4 bytes");
  }

  @Test
  public void readString() {
    var workingStream =
        new BigEndianByteInput(
            new ByteArrayInputStream(new byte[] {0, 0, 0, 6, 65, 66, 67, 32, 66, 66}));
    assertThat(workingStream.readString()).isEqualTo("ABC BB");
  }

  @Test
  public void readRemaining() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    workingStream.readI16();
    assertThat(workingStream.readRemaining()).containsExactly(2, 3, 4, 5, 6, 7);
  }

  @Test
  public void skipBytes() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    workingStream.skipBytes(3);
    assertThat(workingStream.readRemaining()).containsExactly(3, 4, 5, 6, 7);
  }

  @Test
  public void skipBytesTooMany() {
    var bytes = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(bytes));
    assertThatThrownBy(() -> workingStream.skipBytes(10))
        .hasMessageContaining("Unable to skip requested number of bytes");
  }

  @Test
  public void readZeroBytes() {
    var workingStream = new BigEndianByteInput(new ByteArrayInputStream(new byte[] {}));
    assertThat(workingStream.readBytes(0).length).isEqualTo(0);
  }
}
