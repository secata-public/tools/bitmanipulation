package com.secata.stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;

/** Test of {@link LittleEndianByteOutput}. */
public final class LittleEndianByteOutputTest extends ByteOutputTest {

  @Override
  public ByteOutput byteOutput(OutputStream stream) {
    return new LittleEndianByteOutput(stream);
  }

  @Override
  public ByteInput byteInput(byte[] bytes) {
    return new LittleEndianByteInput(new ByteArrayInputStream(bytes));
  }

  @Test
  public void serialize() {
    var referenceData = new byte[] {0, 1, 2, 3, 4, 5, 6, 7};
    byte[] serialize = LittleEndianByteOutput.serialize(stream -> stream.writeBytes(referenceData));
    assertThat(serialize).containsExactly(referenceData);
  }

  @Test
  public void writeI64() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeI64(0x0706050403020100L);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeI64(0), "Unable to write long");
  }

  @Test
  public void writeU64() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeU64(0x0706050403020100L);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeU64(0), "Unable to write long");
  }

  @Test
  public void writeI32() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeI32(0x03020100);
    workingStream.writeI32(0x07060504);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeI32(0), "Unable to write int");
  }

  @Test
  public void writeU32() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeU32(0x03020100);
    workingStream.writeU32(0x07060504);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeU32(0), "Unable to write int");
  }

  @Test
  public void writeI16() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeI16((short) 0x0100);
    workingStream.writeI16((short) 0x0302);
    workingStream.writeI16((short) 0x0504);
    workingStream.writeI16((short) 0x0706);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeI16((short) 0), "Unable to write short");
  }

  @Test
  public void writeU16() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeU16((short) 0x0100);
    workingStream.writeU16((short) 0x0302);
    workingStream.writeU16((short) 0x0504);
    workingStream.writeU16((short) 0x0706);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeU16((short) 0), "Unable to write short");
  }

  @Test
  public void writeI16Int() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeI16(-0x8000);
    workingStream.writeI16(0x7fff);
    var expected = new byte[] {0x00, -0x80, -0x01, 0x7f};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeI16(0), "Unable to write short");
  }

  @Test
  public void writeU16Int() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeU16(0);
    workingStream.writeU16(0xffff);
    var expected = new byte[] {0x00, 0x00, -0x01, -0x01};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeU16(0), "Unable to write short");
  }

  @Test
  public void writeI8() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeI8((byte) 0x00);
    workingStream.writeI8((byte) 0x01);
    workingStream.writeI8((byte) 0x02);
    workingStream.writeI8((byte) 0x03);
    workingStream.writeI8((byte) 0x04);
    workingStream.writeI8((byte) 0x05);
    workingStream.writeI8((byte) 0x06);
    workingStream.writeI8((byte) 0x07);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeI8((byte) 0), "Unable to write byte");
  }

  @Test
  public void writeU8() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeU8((byte) 0x00);
    workingStream.writeU8((byte) 0x01);
    workingStream.writeU8((byte) 0x02);
    workingStream.writeU8((byte) 0x03);
    workingStream.writeU8((byte) 0x04);
    workingStream.writeU8((byte) 0x05);
    workingStream.writeU8((byte) 0x06);
    workingStream.writeU8((byte) 0x07);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeU8((byte) 0), "Unable to write byte");
  }

  @Test
  public void writeI8Int() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeI8(-128);
    workingStream.writeI8(127);
    var expected = new byte[] {-128, 127};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeI8(0), "Unable to write byte");
  }

  @Test
  public void writeU8Int() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeU8(0);
    workingStream.writeU8(255);
    var expected = new byte[] {0, -1};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError((bigEndianByteOutput) -> bigEndianByteOutput.writeU8(0), "Unable to write byte");
  }

  @Test
  public void writeFully() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    var expected = new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    workingStream.writeBytes(expected);
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        bigEndianByteOutput -> bigEndianByteOutput.writeBytes(expected), "Unable to write bytes");
  }

  @Test
  public void writeBoolean() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeBoolean(false);
    workingStream.writeBoolean(true);
    var expected = new byte[] {0x00, 0x01};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeBoolean(false), "Unable to write byte");
  }

  @Test
  void testBigIntegerPos() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeSignedBigInteger(new BigInteger("16"), 2);
    assertThat(stream.toByteArray()).containsExactly(0x10, 0x00);
  }

  @Test
  void testBigIntegerNeg() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeSignedBigInteger(new BigInteger("-16"), 2);
    assertThat(stream.toByteArray()).containsExactly(-0x10, -1);
  }

  @Test
  void testUnsignedBigIntegerSmall() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    var big = BigInteger.valueOf(16);
    workingStream.writeUnsignedBigInteger(big, 16);
    var expected = new byte[] {16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    assertThat(stream.toByteArray()).isEqualTo(expected);
  }

  @Test
  void testUnsignedBigIntegerZero() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    var big = BigInteger.valueOf(0);
    workingStream.writeUnsignedBigInteger(big, 16);
    var expected = new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    assertThat(stream.toByteArray()).isEqualTo(expected);
  }

  @Test
  void writeSignedBigInteger() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    var big = new BigInteger("0F0E0D0C0B0A09080706050403020100", 16);
    workingStream.writeSignedBigInteger(big, 16);
    var expected = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeSignedBigInteger(big, 16),
        "Unable to write bytes");
  }

  @Test
  void writeUnsignedBigInteger() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    var big = new BigInteger("0F0E0D0C0B0A09080706050403020100", 16);
    workingStream.writeUnsignedBigInteger(big, 16);
    var expected = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(
        (bigEndianByteOutput) -> bigEndianByteOutput.writeUnsignedBigInteger(big, 16),
        "Unable to write bytes");
  }

  @Test
  void maxSignedBigInt() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeSignedBigInteger(new BigInteger("32767"), 2);
    var expected = new byte[] {-1, 127};
    assertThat(stream.toByteArray()).isEqualTo(expected);
  }

  @Test
  void tooLargeSignedBigInt() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    assertThatThrownBy(() -> workingStream.writeSignedBigInteger(new BigInteger("32768"), 2))
        .hasMessageContaining("Cannot write BigInteger as 2 bytes; requires at least 3 bytes");
  }

  @Test
  void minSignedBigInt() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeSignedBigInteger(new BigInteger("-32768"), 2);
    var expected = new byte[] {0, -128};
    assertThat(stream.toByteArray()).isEqualTo(expected);
  }

  @Test
  void tooSmallSignedBigInt() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    assertThatThrownBy(() -> workingStream.writeSignedBigInteger(new BigInteger("-32769"), 2))
        .hasMessageContaining("Cannot write BigInteger as 2 bytes; requires at least 3 bytes");
  }

  @Test
  void maxUnsignedBigInt() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeUnsignedBigInteger(new BigInteger("65535"), 2);
    var expected = new byte[] {-1, -1};
    assertThat(stream.toByteArray()).isEqualTo(expected);
  }

  @Test
  void tooLargeUnsignedBigInt() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    assertThatThrownBy(() -> workingStream.writeUnsignedBigInteger(new BigInteger("65536"), 2))
        .hasMessageContaining("Cannot write BigInteger as 2 bytes; requires at least 3 bytes");
  }

  @Test
  public void negativeUnsignedBigint() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    assertThatThrownBy(() -> workingStream.writeUnsignedBigInteger(new BigInteger("-1"), 4))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Cannot write -1 as unsigned: Value must not be negative");
  }

  @Test
  public void writeString() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    workingStream.writeString("ABC BB");
    var expected = new byte[] {6, 0, 0, 0, 65, 66, 67, 32, 66, 66};
    assertThat(stream.toByteArray()).isEqualTo(expected);
    assertError(s -> s.writeString("ABC BB"), "Unable to write int");
  }

  @Test
  public void illegalInputs() {
    var stream = new ByteArrayOutputStream();
    var workingStream = new LittleEndianByteOutput(stream);
    assertThatThrownBy(() -> workingStream.writeI8(128))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -128 and 127, but was 128");
    assertThatThrownBy(() -> workingStream.writeU8(-1))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 255, but was -1");
    assertThatThrownBy(() -> workingStream.writeI16(32768))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -32768 and 32767, but was 32768");
    assertThatThrownBy(() -> workingStream.writeU16(-1))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 65535, but was -1");
    assertThatThrownBy(() -> workingStream.writeI8(-129))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -128 and 127, but was -129");
    assertThatThrownBy(() -> workingStream.writeU8(256))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 255, but was 256");
    assertThatThrownBy(() -> workingStream.writeI16(-32769))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -32768 and 32767, but was -32769");
    assertThatThrownBy(() -> workingStream.writeU16(65536))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 65535, but was 65536");
  }

  private void assertError(Consumer<LittleEndianByteOutput> consumer, String errorMessage) {
    var brokenStream = new LittleEndianByteOutput(null);

    assertThatThrownBy(() -> consumer.accept(brokenStream))
        .hasMessageContaining(errorMessage)
        .isExactlyInstanceOf(RuntimeException.class);
  }
}
