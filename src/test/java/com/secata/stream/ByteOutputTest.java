package com.secata.stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.jupiter.api.Test;

/** Abstract test for testing {@link ByteOutput} implementations. */
public abstract class ByteOutputTest {

  /**
   * Implementations must produce a {@link ByteOutput} for the given stream. Will be used to test
   * {@link ByteOutput} interface compliance.
   *
   * @param stream Stream to create ByteOutput for.
   * @return Produced ByteOutput. Never null.
   */
  public abstract ByteOutput byteOutput(OutputStream stream);

  /**
   * Creates an empty {@link ByteOutput} .
   *
   * @return Newly created empty ByteOutput.
   */
  public final ByteOutput byteOutput() {
    return byteOutput(new ByteArrayOutputStream());
  }

  /**
   * Implementations must produce a {@link ByteInput} for the given bytes. Will be used to test
   * {@link ByteOutput} interface compliance.
   *
   * @param bytes Bytes to create ByteInput for.
   * @return Produced ByteInput. Never null.
   */
  public abstract ByteInput byteInput(byte[] bytes);

  /**
   * Checks that the given {@link Consumer} results in an error when given an broken stream. The
   * broken stream is a stub replicating the scenario when the stream has been cut in some way.
   */
  private AbstractThrowableAssert<?, ? extends Throwable> assertErrorOnBrokenStream(
      Consumer<ByteOutput> consumer) {
    final ByteOutput brokenStream = byteOutput(null);
    return assertThatThrownBy(() -> consumer.accept(brokenStream))
        .isInstanceOf(RuntimeException.class);
  }

  private AbstractThrowableAssert<?, ? extends Throwable> assertError(
      Consumer<ByteOutput> consumer) {
    final ByteOutput stream = byteOutput();
    return assertThatThrownBy(() -> consumer.accept(stream)).isInstanceOf(RuntimeException.class);
  }

  /**
   * Utility asserter testing that all the given examples can be serialized using {@code writer} and
   * then deserialized using {@code reader} without losing an information.
   *
   * @param <T> Type of elements.
   * @param examples Examples to write and read.
   * @param writer Writing method for T.
   * @param reader Reading method for T.
   * @return Returns the length of the serialized array.
   */
  <T> int assertCanReadWhatIsWritten(
      final Iterable<T> examples,
      final BiConsumer<ByteOutput, T> writer,
      final Function<ByteInput, T> reader) {
    final var outputBytes = new ByteArrayOutputStream();

    // Write
    final ByteOutput output = byteOutput(outputBytes);
    for (final T input : examples) {
      writer.accept(output, input);
    }

    // Read
    final byte[] serializedBytes = outputBytes.toByteArray();
    final ByteInput input = byteInput(serializedBytes);
    final List<T> read = new ArrayList<>();
    for (final T unused : examples) {
      read.add(reader.apply(input));
    }

    // Check
    assertThat(read)
        .as("Can read what was written.")
        .usingRecursiveComparison()
        .isEqualTo(examples);

    return serializedBytes.length;
  }

  /** Random generator for use in {@link assertCanWriteWhatIsRead}. */
  private final Random rng = new SecureRandom();

  /**
   * Utility asserter testing that whatever is deserialized by {@code reader} can be re-serialized
   * by {@code writer} to the same byte contents.
   *
   * @param <T> Type of elements.
   * @param numElements Number of elements to read.
   * @param elementSizeBytes Size of each element in bytes.
   * @param writer Writing method for T.
   * @param reader Reading method for T.
   */
  <T> void assertCanWriteWhatIsRead(
      final int numElements,
      final int elementSizeBytes,
      final BiConsumer<ByteOutput, T> writer,
      final Function<ByteInput, T> reader) {

    // Generate bytes
    final byte[] inputBytes = new byte[numElements * elementSizeBytes];
    rng.nextBytes(inputBytes);

    // Read
    final ByteInput input = byteInput(inputBytes);
    final List<T> read = new ArrayList<>();
    for (int idx = 0; idx < numElements; idx++) {
      read.add(reader.apply(input));
    }

    // Write
    final var outputBytes = new ByteArrayOutputStream();
    final ByteOutput output = byteOutput(outputBytes);
    for (final T element : read) {
      writer.accept(output, element);
    }

    // Check
    assertThat(outputBytes.toByteArray())
        .as("Can write what is read: " + read)
        .isEqualTo(inputBytes);
  }

  /**
   * Convenience asserter combining {@link assertCanReadWhatIsWritten} and {@link
   * assertCanWriteWhatIsRead}.
   *
   * @param <T> Type of elements.
   * @param examples Examples for {@link assertCanReadWhatIsWritten}.
   * @param writer Writing method for T.
   * @param reader Reading method for T.
   */
  <T> void assertCanReadReadWriteWrite(
      final Collection<T> examples,
      final BiConsumer<ByteOutput, T> writer,
      final Function<ByteInput, T> reader) {
    final int totalBytesWritten = assertCanReadWhatIsWritten(examples, writer, reader);
    final int numElements = 100;
    final int elementSizeBytes = totalBytesWritten / examples.size();
    assertCanWriteWhatIsRead(numElements, elementSizeBytes, writer, reader);
  }

  /** {@link Long}s can be serialized and deserialized as I64. */
  @Test
  public void writeI64IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.LONGS, ByteOutput::writeI64, ByteInput::readI64);

    assertErrorOnBrokenStream(stream -> stream.writeI64(0))
        .hasMessageContaining("Unable to write long");
  }

  /** {@link Long}s can be serialized and deserialized as U64. */
  @Test
  public void writeU64IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.LONGS, ByteOutput::writeU64, ByteInput::readU64);

    final ByteOutput out = byteOutput();
    out.writeU64(0x0001020304050607L);

    assertErrorOnBrokenStream(stream -> stream.writeU64(0))
        .hasMessageContaining("Unable to write long");
  }

  /** {@link Integer}s can be serialized and deserialized as I32. */
  @Test
  public void writeI32IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.INTEGERS, ByteOutput::writeI32, ByteInput::readI32);

    assertErrorOnBrokenStream(stream -> stream.writeI32(0))
        .hasMessageContaining("Unable to write int");
  }

  /** {@link Integer}s can be serialized and deserialized as U32. */
  @Test
  public void writeU32IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.INTEGERS, ByteOutput::writeU32, ByteInput::readU32);

    assertErrorOnBrokenStream(stream -> stream.writeU32(0))
        .hasMessageContaining("Unable to write int");
  }

  /** {@link Short}s can be serialized and deserialized as I16. */
  @Test
  public void writeI16IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.SHORTS, ByteOutput::writeI16, ByteInput::readI16);

    assertErrorOnBrokenStream(stream -> stream.writeI16((short) 0))
        .hasMessageContaining("Unable to write short");
  }

  /** {@link Short}s can be serialized and deserialized as U16. */
  @Test
  public void writeU16IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.SHORTS, ByteOutput::writeU16, ByteInput::readU16);

    assertErrorOnBrokenStream(stream -> stream.writeU16((short) 0))
        .hasMessageContaining("Unable to write short");
  }

  /** {@link Integer}s can be serialized and deserialized as I16. */
  @Test
  public void writeI16IntIgnoreEndian() {
    final List<Integer> elements = List.of(-0x8000, 0x7fff);
    assertCanReadWhatIsWritten(elements, ByteOutput::writeI16, s -> (int) s.readI16());

    assertErrorOnBrokenStream(stream -> stream.writeI16(0))
        .hasMessageContaining("Unable to write short");
  }

  /** Broken stream gives error when writing U16. */
  @Test
  public void writeU16IntIgnoreEndian() {
    assertErrorOnBrokenStream(stream -> stream.writeU16(0))
        .hasMessageContaining("Unable to write short");
  }

  /** {@link Byte}s can be serialized and deserialized as I8. */
  @Test
  public void writeI8IgnoreEndian() {
    assertCanReadReadWriteWrite(ExampleObjects.BYTES, ByteOutput::writeI8, ByteInput::readI8);

    assertErrorOnBrokenStream(stream -> stream.writeI8((byte) 0))
        .hasMessageContaining("Unable to write byte");
  }

  /** {@link Integer}s can be serialized and deserialized as I8. */
  @Test
  public void writeI8IntIgnoreEndian() {
    final List<Integer> elements = List.of(-128, 127);
    assertCanReadWhatIsWritten(elements, ByteOutput::writeI8, s -> (int) s.readI8());

    assertErrorOnBrokenStream(stream -> stream.writeI8(0))
        .hasMessageContaining("Unable to write byte");
  }

  /** Broken stream gives error when writing U8. */
  @Test
  public void writeU8IntIgnoreEndian() {
    assertErrorOnBrokenStream(stream -> stream.writeU8(0))
        .hasMessageContaining("Unable to write byte");
  }

  /** {@link Byte} arrays can be written and read. */
  @Test
  public void writeFullyIgnoreEndian() {
    assertCanReadReadWriteWrite(
        ExampleObjects.EIGHT_BYTE_ARRAYS, ByteOutput::writeBytes, s -> s.readBytes(8));

    assertErrorOnBrokenStream(stream -> stream.writeBytes(ExampleObjects.EIGHT_BYTE_ARRAYS.get(0)))
        .hasMessageContaining("Unable to write bytes");
  }

  /** {@link Boolean} can be written and read. */
  @Test
  public void writeBooleanIgnoreEndian() {
    assertCanReadWhatIsWritten(
        ExampleObjects.BOOLEANS, ByteOutput::writeBoolean, ByteInput::readBoolean);

    assertErrorOnBrokenStream(stream -> stream.writeBoolean(false))
        .hasMessageContaining("Unable to write byte");
  }

  /** {@link BigInteger}s can be written to {@link ByteOutput}. */
  @Test
  void testBigIntegerPosIgnoreEndian() {

    final ByteOutput out = byteOutput();
    out.writeSignedBigInteger(new BigInteger("16"), 2);
  }

  /** Negative {@link BigInteger}s can be written to {@link ByteOutput}. */
  @Test
  void testBigIntegerNegIgnoreEndian() {

    final ByteOutput out = byteOutput();
    out.writeSignedBigInteger(new BigInteger("-16"), 2);
  }

  /** Negative {@link BigInteger}s can be written to {@link ByteOutput}. */
  @Test
  void writeRead() {
    assertCanReadReadWriteWrite(
        List.of(BigInteger.ZERO),
        (s, i) -> s.writeSignedBigInteger(i, 16),
        s -> s.readSignedBigInteger(16));
    assertCanReadReadWriteWrite(
        List.of(BigInteger.ZERO),
        (s, i) -> s.writeUnsignedBigInteger(i, 16),
        s -> s.readUnsignedBigInteger(16));
    assertCanReadReadWriteWrite(
        ExampleObjects.BIG_INTEGERS_16_POSITIVE,
        (s, i) -> s.writeSignedBigInteger(i, 16),
        s -> s.readSignedBigInteger(16));
    assertCanReadReadWriteWrite(
        ExampleObjects.BIG_INTEGERS_16_POSITIVE,
        (s, i) -> s.writeUnsignedBigInteger(i, 16),
        s -> s.readUnsignedBigInteger(16));
    assertCanReadReadWriteWrite(
        ExampleObjects.BIG_INTEGERS_16_UNSIGNED,
        (s, i) -> s.writeUnsignedBigInteger(i, 16),
        s -> s.readUnsignedBigInteger(16));
  }

  static final BigInteger SOME_BIG_INTEGER = new BigInteger("000102030405060708090A0B0C0D0E0F", 16);

  /** Signed {@link BigInteger}s can be written to {@link ByteOutput}. */
  @Test
  void writeSignedBigIntegerIgnoreEndian() {

    final ByteOutput out = byteOutput();

    // Success
    out.writeSignedBigInteger(SOME_BIG_INTEGER, 16);

    // Error
    assertErrorOnBrokenStream(stream -> stream.writeSignedBigInteger(SOME_BIG_INTEGER, 16))
        .hasMessageContaining("Unable to write bytes");

    // Reference
    for (final BigInteger input : ExampleObjects.BIG_INTEGERS_16_UNSIGNED) {
      assertError(stream -> stream.writeSignedBigInteger(input, 16))
          .hasMessageContaining("Cannot write BigInteger as 16 bytes; requires at least 17 bytes");
    }
  }

  /** Unsigned {@link BigInteger}s can be written to {@link ByteOutput}. */
  @Test
  void writeUnsignedBigIntegerIgnoreEndian() {

    final ByteOutput out = byteOutput();

    // Success
    out.writeUnsignedBigInteger(SOME_BIG_INTEGER, 16);

    // Error
    assertErrorOnBrokenStream(stream -> stream.writeUnsignedBigInteger(SOME_BIG_INTEGER, 16))
        .hasMessageContaining("Unable to write bytes");
  }

  /** Stream can serialize the largest unsigned N-byte number as precisely a N byte output. */
  @Test
  void maxValuesAreSupportedIgnoreEndian() {
    final ByteOutput out = byteOutput();
    for (int numBytes = 1; numBytes <= 100; numBytes++) {
      final BigInteger maxSupportedValue =
          BigInteger.ONE.shiftLeft(8 * numBytes).subtract(BigInteger.ONE);
      out.writeUnsignedBigInteger(maxSupportedValue, numBytes);
    }
  }

  /**
   * Stream will give an error when attempting to write too large of a {@link BigInteger}s with too
   * few bytes.
   */
  @Test
  void tooLargeBigIntIgnoreEndian() {
    assertError(out -> out.writeSignedBigInteger(new BigInteger("128"), 1))
        .hasMessageContaining("Cannot write BigInteger as 1 bytes; requires at least 2 bytes");
    assertError(out -> out.writeSignedBigInteger(new BigInteger("257"), 1))
        .hasMessageContaining("Cannot write BigInteger as 1 bytes; requires at least 2 bytes");

    for (final BigInteger veryBig : ExampleObjects.BIG_INTEGERS_17_UNSIGNED) {
      assertError(out -> out.writeSignedBigInteger(veryBig, 16))
          .hasMessageContaining("Cannot write BigInteger as 16 bytes; requires at least 17 bytes");
      assertError(out -> out.writeUnsignedBigInteger(veryBig, 16))
          .hasMessageContaining("Cannot write BigInteger as 16 bytes; requires at least ");
    }
  }

  /**
   * Stream will give an error when attempting to write a negative {@link BigInteger} as unsigned.
   */
  @Test
  public void negativeUnsignedBigintIgnoreEndian() {
    assertError(stream -> stream.writeUnsignedBigInteger(new BigInteger("-1"), 4))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot write -1 as unsigned: Value must not be negative");

    for (final BigInteger input : ExampleObjects.BIG_INTEGERS_16_POSITIVE) {
      final var negatedInput = input.negate();
      assertError(stream -> stream.writeUnsignedBigInteger(negatedInput, 16))
          .hasMessage("Cannot write " + negatedInput + " as unsigned: Value must not be negative");
    }
  }

  /** Broken stream will produce an error when writing {@link String}s. */
  @Test
  public void writeStringIgnoreEndian() {
    assertErrorOnBrokenStream(s -> s.writeString("ABC BB"))
        .hasMessageContaining("Unable to write int");
  }

  /** Writing methods validate the ranges of values. */
  @Test
  public void illegalInputsIgnoreEndian() {
    assertError(out -> out.writeI8(128))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -128 and 127, but was 128");
    assertError(out -> out.writeU8(-1))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 255, but was -1");
    assertError(out -> out.writeI16(32768))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -32768 and 32767, but was 32768");
    assertError(out -> out.writeU16(-1))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 65535, but was -1");
    assertError(out -> out.writeI8(-129))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -128 and 127, but was -129");
    assertError(out -> out.writeU8(256))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 255, but was 256");
    assertError(out -> out.writeI16(-32769))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between -32768 and 32767, but was -32769");
    assertError(out -> out.writeU16(65536))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Value must be between 0 and 65535, but was 65536");
  }
}
