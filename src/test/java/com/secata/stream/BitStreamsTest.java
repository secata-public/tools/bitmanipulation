package com.secata.stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Test of {@link BitInput} and {@link BitOutput}. */
public final class BitStreamsTest {

  @Test
  void readUnsignedInt() {
    final BitInput stream =
        BitInput.create(new ByteArrayInputStream(new byte[] {(byte) 0xf0, 0x05, (byte) 0xff}));
    assertThat(stream.readUnsignedInt(12)).isEqualTo(0x5f0);
    assertThat(stream.readUnsignedInt(8)).isEqualTo(0xf0);
    assertThat(stream.readUnsignedInt(4)).isEqualTo(0xf);
  }

  @Test
  void readSignedInt() {
    final BitInput stream =
        BitInput.create(new ByteArrayInputStream(new byte[] {(byte) 0xf0, 0x05, (byte) 0xff}));
    assertThat(stream.readSignedInt(12)).isEqualTo(0x5f0);
    assertThat(stream.readSignedInt(8)).isEqualTo(-16);
    assertThat(stream.readSignedInt(4)).isEqualTo(-1);
  }

  @Test
  void writeUnsignedInt() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeUnsignedInt(0x5f0, 12)
                    .writeUnsignedInt(0xf0, 8)
                    .writeUnsignedInt(0xf, 4));
    assertThat(bytes).containsExactly(0xf0, 0x05, 0xff);
  }

  @Test
  void writeSignedInt() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer.writeSignedInt(0x5f0, 12).writeSignedInt(-16, 8).writeSignedInt(-1, 4));
    assertThat(bytes).containsExactly(0xf0, 0x05, 0xff);
  }

  @Test
  void booleans() {
    var booleans = new boolean[] {true, true, false, true, false, false, true, true, false};
    var stream = new ByteArrayOutputStream();
    var writer = new BitOutput(stream);
    for (var bool : booleans) {
      writer.writeBoolean(bool);
    }
    writer.flushLastByte();
    final BitInput reader = BitInput.create(stream.toByteArray());
    for (var bool : booleans) {
      assertThat(reader.readBoolean()).isEqualTo(bool);
    }
  }

  @Test
  void unsignedInt() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeUnsignedInt(42, 31)
                    .writeUnsignedInt(-1, 32)
                    .writeUnsignedInt(31, 5)
                    .writeUnsignedInt(1, 1)
                    .writeUnsignedInt(0, 32)
                    .writeUnsignedInt(3, 17));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readUnsignedInt(31)).isEqualTo(42);
    assertThat(reader.readUnsignedInt(32)).isEqualTo(-1);
    assertThat(reader.readUnsignedInt(5)).isEqualTo(31);
    assertThat(reader.readUnsignedInt(1)).isEqualTo(1);
    assertThat(reader.readUnsignedInt(32)).isEqualTo(0);
    assertThat(reader.readUnsignedInt(17)).isEqualTo(3);
  }

  @Test
  void unsignedLong() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeUnsignedLong(42, 63)
                    .writeUnsignedLong(-1, 64)
                    .writeUnsignedLong(31, 5)
                    .writeUnsignedLong(1, 1)
                    .writeUnsignedLong(0, 64)
                    .writeUnsignedLong(3, 17));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readUnsignedLong(63)).isEqualTo(42);
    assertThat(reader.readUnsignedLong(64)).isEqualTo(-1);
    assertThat(reader.readUnsignedLong(5)).isEqualTo(31);
    assertThat(reader.readUnsignedLong(1)).isEqualTo(1);
    assertThat(reader.readUnsignedLong(64)).isEqualTo(0);
    assertThat(reader.readUnsignedLong(17)).isEqualTo(3);
  }

  @Test
  void unsignedBigInteger() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeUnsignedBigInteger(new BigInteger("256"), 9)
                    .writeUnsignedBigInteger(new BigInteger("999999999999999999999"), 5498)
                    .writeUnsignedBigInteger(BigInteger.ZERO, 9)
                    .writeUnsignedBigInteger(new BigInteger("4294967295"), 32)
                    .writeUnsignedBigInteger(new BigInteger("4294967296"), 33));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readUnsignedBigInteger(9)).isEqualTo(new BigInteger("256"));
    assertThat(reader.readUnsignedBigInteger(5498))
        .isEqualTo(new BigInteger("999999999999999999999"));
    assertThat(reader.readUnsignedBigInteger(9)).isEqualTo(BigInteger.ZERO);
    assertThat(reader.readUnsignedBigInteger(32)).isEqualTo(new BigInteger("4294967295"));
    assertThat(reader.readUnsignedBigInteger(33)).isEqualTo(new BigInteger("4294967296"));
  }

  @Test
  void signedInt() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeSignedInt(-42, 31)
                    .writeSignedInt(Integer.MAX_VALUE, 32)
                    .writeSignedInt(Integer.MIN_VALUE, 32)
                    .writeSignedInt(5, 5)
                    .writeSignedInt(-1, 1)
                    .writeSignedInt(0, 32)
                    .writeSignedInt(-3, 17));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readSignedInt(31)).isEqualTo(-42);
    assertThat(reader.readSignedInt(32)).isEqualTo(Integer.MAX_VALUE);
    assertThat(reader.readSignedInt(32)).isEqualTo(Integer.MIN_VALUE);
    assertThat(reader.readSignedInt(5)).isEqualTo(5);
    assertThat(reader.readSignedInt(1)).isEqualTo(-1);
    assertThat(reader.readSignedInt(32)).isEqualTo(0);
    assertThat(reader.readSignedInt(17)).isEqualTo(-3);
  }

  @Test
  void signedLong() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeSignedLong(-42, 63)
                    .writeSignedLong(Long.MAX_VALUE, 64)
                    .writeSignedLong(Long.MIN_VALUE, 64)
                    .writeSignedLong(5, 5)
                    .writeSignedLong(-1, 1)
                    .writeSignedLong(0, 64)
                    .writeSignedLong(-3, 17));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readSignedLong(63)).isEqualTo(-42);
    assertThat(reader.readSignedLong(64)).isEqualTo(Long.MAX_VALUE);
    assertThat(reader.readSignedLong(64)).isEqualTo(Long.MIN_VALUE);
    assertThat(reader.readSignedLong(5)).isEqualTo(5);
    assertThat(reader.readSignedLong(1)).isEqualTo(-1);
    assertThat(reader.readSignedLong(64)).isEqualTo(0);
    assertThat(reader.readSignedLong(17)).isEqualTo(-3);
  }

  @Test
  void signedBigInteger() {
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer
                    .writeSignedBigInteger(new BigInteger("256"), 10)
                    .writeSignedBigInteger(new BigInteger("-999999999999999999999"), 5498)
                    .writeSignedBigInteger(BigInteger.ZERO, 9)
                    .writeSignedBigInteger(new BigInteger("4294967295"), 33)
                    .writeSignedBigInteger(new BigInteger("-4294967296"), 33));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readSignedBigInteger(10)).isEqualTo(new BigInteger("256"));
    assertThat(reader.readSignedBigInteger(5498))
        .isEqualTo(new BigInteger("-999999999999999999999"));
    assertThat(reader.readSignedBigInteger(9)).isEqualTo(BigInteger.ZERO);
    assertThat(reader.readSignedBigInteger(33)).isEqualTo(new BigInteger("4294967295"));
    assertThat(reader.readSignedBigInteger(33)).isEqualTo(new BigInteger("-4294967296"));
  }

  @Test
  void bytes() {
    final byte[] byteArray =
        BigEndianByteOutput.serialize(
            bytesWriter -> {
              bytesWriter.writeI64(34956034);
              bytesWriter.writeBytes(new byte[9]);
              bytesWriter.writeString("abababab");
              bytesWriter.writeBoolean(true);
              bytesWriter.writeI32(9955);
            });
    final byte[] bytes = BitOutput.serialize(writer -> writer.writeBytes(byteArray));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readBytes(byteArray.length)).isEqualTo(bytes);
  }

  @Test
  void bytesOffset() {
    final byte[] byteArray =
        BigEndianByteOutput.serialize(
            bytesWriter -> {
              bytesWriter.writeI64(34956034);
              bytesWriter.writeBytes(new byte[9]);
              bytesWriter.writeString("abababab");
              bytesWriter.writeBoolean(true);
              bytesWriter.writeI32(9955);
            });
    final byte[] bytes =
        BitOutput.serialize(writer -> writer.writeBoolean(true).writeBytes(byteArray));
    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readBoolean()).isTrue();
    assertThat(reader.readBytes(byteArray.length)).isEqualTo(byteArray);
  }

  @Test
  void invalidNumbers() {
    var writer = new BitOutput(new ByteArrayOutputStream());
    assertThatThrownBy(() -> writer.writeUnsignedBigInteger(new BigInteger("3"), 1))
        .hasMessage("3 cannot be represented as an unsigned 1 bit number");
    assertThatThrownBy(() -> writer.writeUnsignedInt(3, 1))
        .hasMessage("3 cannot be represented as an unsigned 1 bit number");
    assertThatThrownBy(() -> writer.writeUnsignedBigInteger(new BigInteger("-1"), 2))
        .hasMessage("-1 cannot be represented as an unsigned 2 bit number");
    assertThatThrownBy(() -> writer.writeUnsignedInt(-1, 2))
        .hasMessage("-1 cannot be represented as an unsigned 2 bit number");
    assertThatThrownBy(() -> writer.writeUnsignedInt(3, 33))
        .hasMessage(">32 bit numbers must be written using BitOutput#writeUnsignedBigInteger");
    assertThatThrownBy(() -> writer.writeUnsignedLong(3, 65))
        .hasMessage(">64 bit numbers must be written using BitOutput#writeUnsignedBigInteger");

    assertThatThrownBy(() -> writer.writeSignedBigInteger(new BigInteger("3"), 2))
        .hasMessage("3 cannot be represented as a signed 2 bit number");
    assertThatThrownBy(() -> writer.writeSignedBigInteger(new BigInteger("-3"), 2))
        .hasMessage("-3 cannot be represented as a signed 2 bit number");
    assertThatThrownBy(() -> writer.writeSignedInt(3, 2))
        .hasMessage("3 cannot be represented as a signed 2 bit number");
    assertThatThrownBy(() -> writer.writeSignedInt(-3, 2))
        .hasMessage("-3 cannot be represented as a signed 2 bit number");
    assertThatThrownBy(() -> writer.writeSignedInt(3, 33))
        .hasMessage(">32 bit numbers must be written using BitOutput#writeSignedBigInteger");
    assertThatThrownBy(() -> writer.writeSignedLong(3, 65))
        .hasMessage(">64 bit numbers must be written using BitOutput#writeSignedBigInteger");

    final BitInput reader = BitInput.create(new byte[0]);
    assertThatThrownBy(() -> reader.readUnsignedInt(33))
        .hasMessage(">32 bit numbers must be read using BitInput#readUnsignedBigInteger");
    assertThatThrownBy(() -> reader.readSignedInt(33))
        .hasMessage(">32 bit numbers must be read using BitInput#readSignedBigInteger");
    assertThatThrownBy(() -> reader.readUnsignedLong(65))
        .hasMessage(">64 bit numbers must be read using BitInput#readUnsignedBigInteger");
    assertThatThrownBy(() -> reader.readSignedLong(65))
        .hasMessage(">64 bit numbers must be read using BitInput#readSignedBigInteger");
    assertThatThrownBy(reader::readBoolean).hasMessage("Reached end of stream");
    assertThatThrownBy(() -> reader.readBytes(1)).hasMessage("Reached end of stream");
  }

  @Test
  void differentTypes() {
    final byte[] byteArray =
        BigEndianByteOutput.serialize(
            writer -> {
              writer.writeString("abababab");
              writer.writeI32(9955);
            });

    var bits =
        BitOutput.serializeBits(
            writer ->
                writer
                    .writeBoolean(true)
                    .writeUnsignedInt(12, 25)
                    .writeUnsignedBigInteger(new BigInteger("94857694857649856749867"), 240)
                    .writeSignedInt(15, 5)
                    .writeBoolean(false)
                    .writeBytes(byteArray)
                    .writeSignedBigInteger(new BigInteger("1234567890"), 63)
                    .writeUnsignedLong(255, 8));

    assertThat(bits.size()).isEqualTo(1 + 25 + 240 + 5 + 1 + byteArray.length * 8 + 63 + 8);
    final BitInput reader = BitInput.create(bits.data());
    assertThat(reader.readBoolean()).isEqualTo(true);
    assertThat(reader.readUnsignedInt(25)).isEqualTo(12);
    assertThat(reader.readUnsignedBigInteger(240))
        .isEqualTo(new BigInteger("94857694857649856749867"));
    assertThat(reader.readSignedInt(5)).isEqualTo(15);
    assertThat(reader.readBoolean()).isEqualTo(false);
    assertThat(reader.readBytes(byteArray.length)).isEqualTo(byteArray);
    assertThat(reader.readSignedBigInteger(63)).isEqualTo(new BigInteger("1234567890"));
    assertThat(reader.readUnsignedLong(8)).isEqualTo(255);
    assertThatThrownBy(() -> reader.readUnsignedInt(4)).hasMessage("Reached end of stream");
  }

  @Test
  void writtenBits() {
    var writer = new BitOutput(new ByteArrayOutputStream()).writeBoolean(true);
    assertThat(writer.writtenBits()).isEqualTo(1);
    writer.writeUnsignedInt(98, 7);
    assertThat(writer.writtenBits()).isEqualTo(1 + 7);
    writer.writeBoolean(true);
    assertThat(writer.writtenBits()).isEqualTo(1 + 7 + 1);
    writer.writeUnsignedBigInteger(new BigInteger("348957"), 29);
    assertThat(writer.writtenBits()).isEqualTo(1 + 7 + 1 + 29);
    writer.flushLastByte();
    assertThat(writer.writtenBits()).isEqualTo(1 + 7 + 1 + 29 + 2);
  }

  @Test
  void partOfByteArray() {
    final byte[] byteArray = new byte[] {1, 2, 3, 4, 5, 6, 7};
    final byte[] bytes =
        BitOutput.serialize(
            writer ->
                writer.writeBytes(byteArray, 2, 3).writeBoolean(true).writeBytes(byteArray, 2, 3));

    final BitInput reader = BitInput.create(bytes);
    assertThat(reader.readBytes(3)).containsExactly(3, 4, 5);
    assertThat(reader.readBoolean()).isTrue();
    assertThat(reader.readBytes(3)).containsExactly(3, 4, 5);
  }

  @Test
  void illegalCompactBitArray() {
    assertThatThrownBy(() -> new CompactBitArray(new byte[2], 4))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Size doesn't match length of the data");
    assertThatThrownBy(() -> new CompactBitArray(new byte[2], 20))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Size doesn't match length of the data");
  }
}
