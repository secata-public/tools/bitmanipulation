package com.secata.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

/** Test. */
public class ByteHelperTest {

  @Test
  public void tripleXor() {
    byte[] bytes = new byte[32];
    Arrays.fill(bytes, (byte) 13);

    assertThat(ByteHelper.xor(bytes, bytes, bytes)).isEqualTo(bytes);
  }

  @Test
  public void xorDifferent() {
    byte[] left = new byte[5];
    byte[] right = new byte[5];
    byte[] expectedResult = new byte[5];
    for (int i = 0; i < left.length; i++) {
      byte leftValue = (byte) (i + 7);
      byte rightValue = (byte) (i * 3);
      left[i] = leftValue;
      right[i] = rightValue;
      expectedResult[i] = (byte) (leftValue ^ rightValue);
    }

    byte[] result = ByteHelper.xor(left, right);
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  public void xor() {
    final byte[] left = new byte[32];
    byte leftValue = (byte) -3;
    Arrays.fill(left, leftValue);
    final byte[] expectedLeft = left.clone();
    final byte[] right = new byte[32];
    byte rightValue = (byte) 5;
    Arrays.fill(right, rightValue);
    final byte[] expectedRight = right.clone();

    final byte[] result = ByteHelper.xor(left, right);
    final byte[] expectedResult = new byte[32];
    Arrays.fill(expectedResult, (byte) (leftValue ^ rightValue));

    assertThat(left).isEqualTo(expectedLeft);
    assertThat(right).isEqualTo(expectedRight);
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  public void xorInPlace() {
    final byte[] left = new byte[32];
    byte leftValue = (byte) -3;
    Arrays.fill(left, leftValue);
    final byte[] right = new byte[32];
    byte rightValue = (byte) 5;
    Arrays.fill(right, rightValue);
    final byte[] expectedRight = right.clone();

    ByteHelper.xorInPlace(left, right);
    final byte[] expectedResult = new byte[32];
    Arrays.fill(expectedResult, (byte) (leftValue ^ rightValue));

    assertThat(left).isEqualTo(expectedResult);
    assertThat(right).isEqualTo(expectedRight);
  }

  @Test
  public void shiftLeftEmptyFirst() {
    byte[] byteArray = new byte[] {0, 0b00000010};
    byte[] bytes = ByteHelper.shiftLeft(byteArray, 8);
    assertThat(bytes).isEqualTo(new byte[] {0b00000010, 0});
  }

  @Test
  public void shiftLeftWithCarry() {
    byte[] byteArray = new byte[] {0, -1};
    byte[] bytes = ByteHelper.shiftLeft(byteArray, 4);
    assertThat(bytes).isEqualTo(new byte[] {0b00001111, (byte) 0b11110000});
  }

  @Test
  public void shiftLeft() {
    byte[] byteArray = new byte[] {0, 0b01100110};
    byte[] bytes = ByteHelper.shiftLeft(byteArray, 3);
    assertThat(bytes).isEqualTo(new byte[] {0b00000011, 0b00110000});
  }

  @Test
  public void lastBit() {
    assertThat(ByteHelper.lastBit(new byte[] {0, 0x00000001})).isEqualTo(1);
    assertThat(ByteHelper.lastBit(new byte[] {-1, 0x00000010})).isEqualTo(0);
  }

  @Test
  public void incrementToZero() {
    byte[] bytes = {-1, -1};
    ByteHelper.increment(bytes);
    assertThat(bytes).isEqualTo(new byte[] {0, 0});
  }

  @Test
  public void increment() {
    byte[] bytes = {0, 5};
    ByteHelper.increment(bytes);
    assertThat(bytes).isEqualTo(new byte[] {0, 6});
  }

  @Test
  public void reverse() {
    final byte[] bytes = {1, 2, 3, 4, 5};
    final byte[] result = ByteHelper.reverse(bytes);
    assertThat(result).isEqualTo(new byte[] {5, 4, 3, 2, 1});
  }
}
