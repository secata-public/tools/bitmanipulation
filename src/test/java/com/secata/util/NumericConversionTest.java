package com.secata.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Test. */
public class NumericConversionTest {

  @Test
  public void longToBytesNoBuffer() {
    byte[] bytes = NumericConversion.longToBytes(-1);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1, -1, -1, -1, -1, -1, -1});
  }

  @Test
  public void longToBytesNegative() {
    byte[] bytes = new byte[8];
    NumericConversion.longToBytes(-1L, bytes, 0);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1, -1, -1, -1, -1, -1, -1});
  }

  @Test
  public void longToBytes() {
    byte[] bytes = new byte[10];
    bytes[0] = 5;
    bytes[1] = 7;
    NumericConversion.longToBytes(Long.MAX_VALUE, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, Byte.MAX_VALUE, -1, -1, -1, -1, -1, -1, -1});
  }

  @Test
  public void longFromBytes() {
    long value =
        NumericConversion.longFromBytes(
            new byte[] {6, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, 13}, 3);
    assertThat(value).isEqualTo(-1L);
  }

  @Test
  public void longFromBytesWithOffset() {
    long expected = 0xABCDEF0011223344L;
    byte[] buffer =
        new byte[] {
          (byte) 0x42,

          // Number starts here
          (byte) 0xAB,
          (byte) 0xCD,
          (byte) 0xEF,
          (byte) 0x00,
          (byte) 0x11,
          (byte) 0x22,
          (byte) 0x33,
          (byte) 0x44
        };

    long actual = NumericConversion.longFromBytes(buffer, 1);
    assertThat(expected).isEqualTo(actual);
  }

  @Test
  public void intToBytesNoBuffer() {
    byte[] bytes = NumericConversion.intToBytes(-1);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1, -1, -1});
  }

  @Test
  public void intToBytes() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    NumericConversion.intToBytes(Integer.MAX_VALUE, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, Byte.MAX_VALUE, -1, -1, -1, 33});
  }

  @Test
  public void intFromBytes() {
    int value = NumericConversion.intFromBytes(new byte[] {6, 6, 7, -1, -1, -1, -1, 13}, 3);
    assertThat(value).isEqualTo(-1);
  }

  @Test
  public void shortToBytesNoBuffer() {
    byte[] bytes = NumericConversion.shortToBytes((byte) -1);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1});
  }

  @Test
  public void shortToBytes() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    NumericConversion.shortToBytes(Short.MAX_VALUE, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, Byte.MAX_VALUE, -1, 0, 0, 33});
  }

  @Test
  public void shortFromBytes() {
    int value = NumericConversion.shortFromBytes(new byte[] {6, 6, 7, -1, -1, -1, -1, 13}, 3);
    assertThat(value).isEqualTo(-1);
  }

  @Test
  public void bigIntToBytes() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    NumericConversion.bigIntToBytes(new BigInteger("2147483647"), 4, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, 127, -1, -1, -1, 33});
  }

  @Test
  public void bigIntToBytesNegative() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    NumericConversion.bigIntToBytes(new BigInteger("-1"), 4, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, -1, -1, -1, -1, 33});
  }

  @Test
  public void bigIntToBytesError() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    assertThatThrownBy(
            () -> NumericConversion.bigIntToBytes(new BigInteger("2147483648"), 4, bytes, 2))
        .hasMessageContaining("Cannot write BigInteger as 4 bytes; requires at least 5 bytes");
  }
}
