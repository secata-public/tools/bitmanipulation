package com.secata.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/** Test. */
public class ShareConversionTest {

  @Test
  public void longToBytesNoBuffer() {
    byte[] bytes = ShareConversion.longToBytes(-1);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1, -1, -1, -1, -1, -1, -1});
  }

  @Test
  public void longToBytesNegative() {
    byte[] bytes = new byte[8];
    ShareConversion.longToBytes(-1L, bytes, 0);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1, -1, -1, -1, -1, -1, -1});
  }

  @Test
  public void longToBytes() {
    byte[] bytes = new byte[10];
    bytes[0] = 5;
    bytes[1] = 7;
    ShareConversion.longToBytes(Long.MAX_VALUE, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, -1, -1, -1, -1, -1, -1, -1, Byte.MAX_VALUE});
  }

  @Test
  public void longFromBytes() {
    long value =
        ShareConversion.longFromBytes(new byte[] {6, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, 13}, 3);
    assertThat(value).isEqualTo(-1L);
  }

  @Test
  public void longFromBytesWithOffset() {
    long expected = 0xABCDEF0011223344L;
    byte[] buffer =
        new byte[] {
          (byte) 0x42,

          // Number starts here
          (byte) 0x44,
          (byte) 0x33,
          (byte) 0x22,
          (byte) 0x11,
          (byte) 0x00,
          (byte) 0xEF,
          (byte) 0xCD,
          (byte) 0xAB
        };

    long actual = ShareConversion.longFromBytes(buffer, 1);
    assertThat(expected).isEqualTo(actual);
  }

  @Test
  public void intToBytesNoBuffer() {
    byte[] bytes = ShareConversion.intToBytes(-1);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1, -1, -1});
  }

  @Test
  public void intToBytes() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    ShareConversion.intToBytes(Integer.MAX_VALUE, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, -1, -1, -1, Byte.MAX_VALUE, 33});
  }

  @Test
  public void intFromBytes() {
    int value = ShareConversion.intFromBytes(new byte[] {6, 6, 7, -1, -1, -1, -1, 13}, 3);
    assertThat(value).isEqualTo(-1);
  }

  @Test
  public void shortToBytesNoBuffer() {
    byte[] bytes = ShareConversion.shortToBytes((byte) -1);
    assertThat(bytes).isEqualTo(new byte[] {-1, -1});
  }

  @Test
  public void shortToBytes() {
    byte[] bytes = new byte[7];
    bytes[0] = 5;
    bytes[1] = 7;
    bytes[6] = 33;
    ShareConversion.shortToBytes(Short.MAX_VALUE, bytes, 2);
    assertThat(bytes).isEqualTo(new byte[] {5, 7, -1, Byte.MAX_VALUE, 0, 0, 33});
  }

  @Test
  public void shortFromBytes() {
    int value = ShareConversion.shortFromBytes(new byte[] {6, 6, 7, -1, -1, -1, -1, 13}, 3);
    assertThat(value).isEqualTo(-1);
  }
}
