package com.secata.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/** Test. */
public class BitHelperTest {

  @Test
  public void isLongBitSetPositive() {
    assertThat(BitHelper.isLongBitSet(0b0001L, 0)).isTrue();
    assertThat(BitHelper.isLongBitSet(-1L, 63)).isTrue();
    assertThat(BitHelper.isLongBitSet(-1L, 0)).isTrue();
    assertThat(BitHelper.isLongBitSet(Long.MAX_VALUE, 62)).isTrue();
  }

  @Test
  public void isLongBitSetNegative() {
    assertThat(BitHelper.isLongBitSet(0, 0)).isFalse();
    assertThat(BitHelper.isLongBitSet(0x0001L, 1)).isFalse();
    assertThat(BitHelper.isLongBitSet(-2L, 0)).isFalse();
    assertThat(BitHelper.isLongBitSet(Long.MAX_VALUE, 63)).isFalse();
  }

  @Test
  public void isLongBitSetOutOfRange() {
    assertThat(BitHelper.isLongBitSet(0b111L, 0)).isTrue();
    assertThat(BitHelper.isLongBitSet(0b111L, 1)).isTrue();
    assertThat(BitHelper.isLongBitSet(0b111L, 2)).isTrue();
    for (int i = 3; i < 1000; i++) {
      assertThat(BitHelper.isLongBitSet(0b111L, i)).as("Bit " + i).isFalse();
    }
  }

  @Test
  @SuppressWarnings("deprecation")
  public void isBitSetPositive() {
    assertThat(BitHelper.isBitSet(0b0001L, 0)).isTrue();
    assertThat(BitHelper.isBitSet(-1L, 63)).isTrue();
    assertThat(BitHelper.isBitSet(-1L, 0)).isTrue();
    assertThat(BitHelper.isBitSet(Long.MAX_VALUE, 62)).isTrue();
  }

  @Test
  @SuppressWarnings("deprecation")
  public void isBitSetNegative() {
    assertThat(BitHelper.isBitSet(0, 0)).isFalse();
    assertThat(BitHelper.isBitSet(0x0001L, 1)).isFalse();
    assertThat(BitHelper.isBitSet(-2L, 0)).isFalse();
    assertThat(BitHelper.isBitSet(Long.MAX_VALUE, 63)).isFalse();
  }

  @Test
  @SuppressWarnings("deprecation")
  public void isBitSetOutOfRange() {
    assertThat(BitHelper.isBitSet(0b111L, 0)).isTrue();
    assertThat(BitHelper.isBitSet(0b111L, 1)).isTrue();
    assertThat(BitHelper.isBitSet(0b111L, 2)).isTrue();
    assertThat(BitHelper.isBitSet(0b111L, 3)).isFalse();
    assertThat(BitHelper.isBitSet(0b111L, 4)).isFalse();
    assertThat(BitHelper.isBitSet(0b111L, 64)).isTrue();
    assertThat(BitHelper.isBitSet(0b111L, 65)).isTrue();
    assertThat(BitHelper.isBitSet(0b111L, 66)).isTrue();
    assertThat(BitHelper.isBitSet(0b111L, 67)).isFalse();
    assertThat(BitHelper.isBitSet(0b111L, 68)).isFalse();
  }
}
