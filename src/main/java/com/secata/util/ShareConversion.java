package com.secata.util;

/** Utility class for converting numbers using little endian. */
public final class ShareConversion {

  private ShareConversion() {}

  /**
   * Convert the supplied int to bytes.
   *
   * @param value the value to convert
   * @return a byte array of length 4 containing the same bits as the supplied int.
   */
  public static byte[] intToBytes(int value) {
    byte[] writeBuffer = new byte[4];
    intToBytes(value, writeBuffer, 0);
    return writeBuffer;
  }

  /**
   * Convert the supplied int to bytes.
   *
   * @param value the value to convert
   * @param writeBuffer the array to write the bytes to, should have length of at least offset+4
   * @param offset the starting index to write the bytes
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static void intToBytes(int value, byte[] writeBuffer, int offset) {
    writeBuffer[3 + offset] = (byte) (value >>> 24);
    writeBuffer[2 + offset] = (byte) (value >>> 16);
    writeBuffer[1 + offset] = (byte) (value >>> 8);
    writeBuffer[0 + offset] = (byte) value;
  }

  /**
   * Convert the given bytes to the corresponding int value.
   *
   * @param bytes the bytes to convert. The int will be read from index offset to offset+4.
   * @param offset the offset into the byte array where the int is read from
   * @return the reconstructed int value
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static int intFromBytes(byte[] bytes, int offset) {
    return Byte.toUnsignedInt(bytes[3 + offset]) << 24
        | Byte.toUnsignedInt(bytes[2 + offset]) << 16
        | Byte.toUnsignedInt(bytes[1 + offset]) << 8
        | Byte.toUnsignedInt(bytes[0 + offset]);
  }

  /**
   * Convert the supplied long to bytes.
   *
   * @param value the value to convert
   * @return a byte array of length 8 containing the same bits as the supplied long.
   */
  public static byte[] longToBytes(long value) {
    byte[] writeBuffer = new byte[8];
    longToBytes(value, writeBuffer, 0);
    return writeBuffer;
  }

  /**
   * Convert the supplied long to bytes.
   *
   * @param value the value to convert
   * @param writeBuffer the array to write the bytes to, should have length of at least offset+8
   * @param offset the starting index to write the bytes
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static void longToBytes(long value, byte[] writeBuffer, int offset) {
    writeBuffer[7 + offset] = (byte) (value >>> 56);
    writeBuffer[6 + offset] = (byte) (value >>> 48);
    writeBuffer[5 + offset] = (byte) (value >>> 40);
    writeBuffer[4 + offset] = (byte) (value >>> 32);
    writeBuffer[3 + offset] = (byte) (value >>> 24);
    writeBuffer[2 + offset] = (byte) (value >>> 16);
    writeBuffer[1 + offset] = (byte) (value >>> 8);
    writeBuffer[0 + offset] = (byte) value;
  }

  /**
   * Convert the given bytes to the corresponding long value.
   *
   * @param bytes the bytes to convert. The long will be read from index offset to offset+8.
   * @param offset the offset into the byte array where the long is read from
   * @return the reconstructed long value
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static long longFromBytes(byte[] bytes, int offset) {
    return Byte.toUnsignedLong(bytes[7 + offset]) << 56
        | Byte.toUnsignedLong(bytes[6 + offset]) << 48
        | Byte.toUnsignedLong(bytes[5 + offset]) << 40
        | Byte.toUnsignedLong(bytes[4 + offset]) << 32
        | Byte.toUnsignedLong(bytes[3 + offset]) << 24
        | Byte.toUnsignedLong(bytes[2 + offset]) << 16
        | Byte.toUnsignedLong(bytes[1 + offset]) << 8
        | Byte.toUnsignedLong(bytes[0 + offset]);
  }

  /**
   * Convert the supplied short to bytes.
   *
   * @param value the value to convert
   * @return a byte array of length 2 containing the same bits as the supplied short.
   */
  public static byte[] shortToBytes(short value) {
    byte[] writeBuffer = new byte[2];
    shortToBytes(value, writeBuffer, 0);
    return writeBuffer;
  }

  /**
   * Convert the supplied short to bytes.
   *
   * @param value the value to convert
   * @param writeBuffer the array to write the bytes to, should have length of at least offset+2
   * @param offset the starting index to write the bytes
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static void shortToBytes(short value, byte[] writeBuffer, int offset) {
    writeBuffer[1 + offset] = (byte) (value >>> 8);
    writeBuffer[0 + offset] = (byte) value;
  }

  /**
   * Convert the given bytes to the corresponding short value.
   *
   * @param bytes the bytes to convert. The short will be read from index offset to offset+2.
   * @param offset the offset into the byte array where the short is read from
   * @return the reconstructed short value
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static short shortFromBytes(byte[] bytes, int offset) {
    return (short)
        (Byte.toUnsignedInt(bytes[1 + offset]) << 8 | Byte.toUnsignedInt(bytes[0 + offset]));
  }
}
