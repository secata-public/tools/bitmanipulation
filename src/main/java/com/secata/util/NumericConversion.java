package com.secata.util;

import java.math.BigInteger;
import java.util.Arrays;

/** Utility class for different conversions of numbers using big endian. */
public final class NumericConversion {

  private NumericConversion() {}

  /**
   * Convert the supplied int to bytes.
   *
   * @param value the value to convert
   * @return a byte array of length 4 containing the same bits as the supplied int.
   */
  public static byte[] intToBytes(int value) {
    byte[] writeBuffer = new byte[4];
    intToBytes(value, writeBuffer, 0);
    return writeBuffer;
  }

  /**
   * Convert the supplied int to bytes.
   *
   * @param value the value to convert
   * @param writeBuffer the array to write the bytes to, should have length of at least offset+4
   * @param offset the starting index to write the bytes
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static void intToBytes(int value, byte[] writeBuffer, int offset) {
    writeBuffer[0 + offset] = (byte) (value >>> 24);
    writeBuffer[1 + offset] = (byte) (value >>> 16);
    writeBuffer[2 + offset] = (byte) (value >>> 8);
    writeBuffer[3 + offset] = (byte) value;
  }

  /**
   * Convert the given bytes to the corresponding int value.
   *
   * @param bytes the bytes to convert. The int will be read from index offset to offset+4.
   * @param offset the offset into the byte array where the int is read from
   * @return the reconstructed int value
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static int intFromBytes(byte[] bytes, int offset) {
    return Byte.toUnsignedInt(bytes[0 + offset]) << 24
        | Byte.toUnsignedInt(bytes[1 + offset]) << 16
        | Byte.toUnsignedInt(bytes[2 + offset]) << 8
        | Byte.toUnsignedInt(bytes[3 + offset]);
  }

  /**
   * Convert the supplied long to bytes.
   *
   * @param value the value to convert
   * @return a byte array of length 8 containing the same bits as the supplied long.
   */
  public static byte[] longToBytes(long value) {
    byte[] writeBuffer = new byte[8];
    longToBytes(value, writeBuffer, 0);
    return writeBuffer;
  }

  /**
   * Convert the supplied long to bytes.
   *
   * @param value the value to convert
   * @param writeBuffer the array to write the bytes to, should have length of at least offset+8
   * @param offset the starting index to write the bytes
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static void longToBytes(long value, byte[] writeBuffer, int offset) {
    writeBuffer[0 + offset] = (byte) (value >>> 56);
    writeBuffer[1 + offset] = (byte) (value >>> 48);
    writeBuffer[2 + offset] = (byte) (value >>> 40);
    writeBuffer[3 + offset] = (byte) (value >>> 32);
    writeBuffer[4 + offset] = (byte) (value >>> 24);
    writeBuffer[5 + offset] = (byte) (value >>> 16);
    writeBuffer[6 + offset] = (byte) (value >>> 8);
    writeBuffer[7 + offset] = (byte) value;
  }

  /**
   * Convert the given bytes to the corresponding long value.
   *
   * @param bytes the bytes to convert. The long will be read from index offset to offset+8.
   * @param offset the offset into the byte array where the long is read from
   * @return the reconstructed long value
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static long longFromBytes(byte[] bytes, int offset) {
    return Byte.toUnsignedLong(bytes[0 + offset]) << 56
        | Byte.toUnsignedLong(bytes[1 + offset]) << 48
        | Byte.toUnsignedLong(bytes[2 + offset]) << 40
        | Byte.toUnsignedLong(bytes[3 + offset]) << 32
        | Byte.toUnsignedLong(bytes[4 + offset]) << 24
        | Byte.toUnsignedLong(bytes[5 + offset]) << 16
        | Byte.toUnsignedLong(bytes[6 + offset]) << 8
        | Byte.toUnsignedLong(bytes[7 + offset]);
  }

  /**
   * Convert the supplied short to bytes.
   *
   * @param value the value to convert
   * @return a byte array of length 2 containing the same bits as the supplied short.
   */
  public static byte[] shortToBytes(short value) {
    byte[] writeBuffer = new byte[2];
    shortToBytes(value, writeBuffer, 0);
    return writeBuffer;
  }

  /**
   * Convert the supplied short to bytes.
   *
   * @param value the value to convert
   * @param writeBuffer the array to write the bytes to, should have length of at least offset+2
   * @param offset the starting index to write the bytes
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static void shortToBytes(short value, byte[] writeBuffer, int offset) {
    writeBuffer[0 + offset] = (byte) (value >>> 8);
    writeBuffer[1 + offset] = (byte) value;
  }

  /**
   * Convert the given bytes to the corresponding short value.
   *
   * @param bytes the bytes to convert. The short will be read from index offset to offset+2.
   * @param offset the offset into the byte array where the short is read from
   * @return the reconstructed short value
   */
  @SuppressWarnings({"PointlessArithmeticExpression"})
  public static short shortFromBytes(byte[] bytes, int offset) {
    return (short)
        (Byte.toUnsignedInt(bytes[0 + offset]) << 8 | Byte.toUnsignedInt(bytes[1 + offset]));
  }

  /**
   * Convert the supplied signed {@link BigInteger} to bytes with length {@code desiredByteLength}.
   * Will sign extend for the remaining bytes.
   *
   * @param value the value to write as bytes. Not nullable.
   * @param desiredByteLength the number of bytes to write the number as. Cannot be negative.
   * @param writeBuffer the array to write the bytes to. Should have length of at least {@code
   *     offset + desiredByteLength}. Not nullable.
   * @param offset the index within {@code writeBuffer} to write the first byte. Cannot be negative.
   */
  public static void bigIntToBytes(
      BigInteger value, int desiredByteLength, byte[] writeBuffer, int offset) {
    final byte[] directSerialization = value.toByteArray();
    final byte signByte = (byte) (value.signum() == -1 ? -1 : 0);
    writeBigIntBytesAndExtend(
        directSerialization, desiredByteLength, writeBuffer, offset, signByte);
  }

  /**
   * Convert the supplied unsigned {@link BigInteger} to bytes with length {@code
   * desiredByteLength}.
   *
   * @param value the value to write as bytes. Not nullable.
   * @param desiredByteLength the number of bytes to write the number as. Cannot be negative.
   * @param writeBuffer the array to write the bytes to. Should have length of at least {@code
   *     offset + desiredByteLength}. Not nullable.
   * @param offset the index within {@code writeBuffer} to write the first byte. Cannot be negative.
   */
  public static void bigIntToBytesUnsigned(
      BigInteger value, int desiredByteLength, byte[] writeBuffer, int offset) {
    byte[] directSerialization = value.toByteArray();
    if (directSerialization[0] == 0) {
      directSerialization = Arrays.copyOfRange(directSerialization, 1, directSerialization.length);
    }
    writeBigIntBytesAndExtend(
        directSerialization, desiredByteLength, writeBuffer, offset, (byte) 0);
  }

  /**
   * Copies {@code bytes} into {@code writeBuffer}. Prepends bytes with the extend byte such that
   * the total bytes written are {@code desiredByteLength}: {@code extendByte + ... + extendByte +
   * bytes}.
   *
   * @param bytes the bytes to write to the {@code writeBuffer}. Not nullable.
   * @param desiredByteLength the number of bytes to write the number as. Cannot be negative.
   * @param writeBuffer the array to write the bytes to. Should have length of at least {@code
   *     offset + desiredByteLength}. Not nullable.
   * @param offset the index within {@code writeBuffer} to write the first byte. Cannot be negative.
   * @param extendByte the extend byte to pad the bytes with.
   * @throws IllegalArgumentException if {@code bytes.length} is longer than {@code
   *     desiredByteLength}.
   */
  private static void writeBigIntBytesAndExtend(
      byte[] bytes, int desiredByteLength, byte[] writeBuffer, int offset, byte extendByte) {
    if (desiredByteLength < bytes.length) {
      throw new IllegalArgumentException(
          "Cannot write BigInteger as %d bytes; requires at least %d bytes"
              .formatted(desiredByteLength, bytes.length));
    }
    Arrays.fill(writeBuffer, offset, offset + desiredByteLength - bytes.length, extendByte);
    System.arraycopy(
        bytes, 0, writeBuffer, offset + desiredByteLength - bytes.length, bytes.length);
  }
}
