package com.secata.util;

/** Utility method for checking bits in long. */
public final class BitHelper {

  private BitHelper() {}

  /**
   * Compute whether a given bit is set in the supplied long. The index will never wrap, unlike
   * {@link BitHelper#isBitSetWrapping}.
   *
   * @param value the value to get a bit from
   * @param bit the bit to get
   * @return true if the bit at index bit is set. False if bit index is higher than 63.
   */
  public static boolean isLongBitSet(long value, int bit) {
    return bit < Long.SIZE && isBitSetWrapping(value, bit);
  }

  /**
   * Compute whether a given bit is set in the supplied long. The index will wrap, such that {@code
   * isBitSet(l, i) == isBitSet(l, i + 64)}. Backwards-compatible alias for {@link
   * BitHelper#isBitSetWrapping}.
   *
   * @param value the value to get a bit from
   * @param bit the bit to get
   * @return true if the bit at index bit is set.
   * @deprecated use either {@link BitHelper#isLongBitSet} or {@link BitHelper#isBitSetWrapping}
   *     depending upon desired behaviour.
   */
  @Deprecated(since = "3.1.2")
  @SuppressWarnings("InlineMeSuggester")
  public static boolean isBitSet(long value, int bit) {
    return isBitSetWrapping(value, bit);
  }

  /**
   * Compute whether a given bit is set in the supplied long. The index will wrap, such that {@code
   * isBitSet(l, i) == isBitSet(l, i + 64)}.
   *
   * @param value the value to get a bit from
   * @param bit the bit to get
   * @return true if the bit at index bit is set.
   */
  public static boolean isBitSetWrapping(long value, int bit) {
    long l = value & (1L << bit);
    return l != 0;
  }
}
