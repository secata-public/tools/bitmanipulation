package com.secata.util;

/** Utility methods for manipulating byte arrays. */
public final class ByteHelper {

  private ByteHelper() {}

  /**
   * Compute the xor of two byte arrays.
   *
   * @param left left side of operation
   * @param right right side of operation
   * @return the a new byte array containing the xor
   */
  public static byte[] xor(byte[] left, byte[] right) {
    byte[] result = left.clone();
    xorInPlace(result, right);
    return result;
  }

  /**
   * Compute the xor of three byte arrays.
   *
   * @param array1 the first array.
   * @param array2 the second array.
   * @param array3 the third array.
   * @return the a new byte array containing the xor
   */
  public static byte[] xor(byte[] array1, byte[] array2, byte[] array3) {
    byte[] result = xor(array1, array2);
    xorInPlace(result, array3);
    return result;
  }

  /**
   * Compute the xor of two arrays, mutating the first. The size of other must be at least as large
   * as target.
   *
   * @param target the left side of the operation, and target of the result
   * @param other the right side of the operation
   */
  public static void xorInPlace(byte[] target, byte[] other) {
    for (int i = 0; i < target.length; i++) {
      target[i] ^= other[i];
    }
  }

  /**
   * Shift the value of the byte array left by a given number of bits.
   *
   * @param byteArray the byte array to shift
   * @param shiftBitCount the number of bits to shift the array
   * @return a new array containing the shifted value
   */
  public static byte[] shiftLeft(byte[] byteArray, int shiftBitCount) {
    byte[] result = new byte[byteArray.length];
    final int shiftMod = shiftBitCount % 8;
    final byte carryMask = (byte) ((1 << shiftMod) - 1);
    final int offsetBytes = shiftBitCount / 8;

    for (int sourceIndex = offsetBytes; sourceIndex < byteArray.length; sourceIndex++) {
      byte src = byteArray[sourceIndex];
      byte dst = (byte) (src << shiftMod);
      if (sourceIndex + 1 < byteArray.length) {
        dst = (byte) (dst | ((byteArray[sourceIndex + 1] >>> (8 - shiftMod)) & carryMask));
      }
      result[sourceIndex - offsetBytes] = dst;
    }
    return result;
  }

  /**
   * Get the value of the last bit of a byte array.
   *
   * @param bytes the byte array to get last bit for
   * @return 1 if the last bit of bytes is 1, 0 otherwise
   */
  public static int lastBit(byte[] bytes) {
    return bytes[bytes.length - 1] & 1;
  }

  /**
   * Consider the bytes as representing a number and increment it by one.
   *
   * @param bytes the array to increment
   */
  public static void increment(byte[] bytes) {
    for (int i = bytes.length - 1; i >= 0; i--) {
      if (bytes[i] != -1) {
        bytes[i]++;
        return;
      } else {
        bytes[i] = 0;
      }
    }
  }

  /**
   * Reverses the byte array.
   *
   * @param bytes the byte array to be reversed
   * @return the reversed byte array
   */
  public static byte[] reverse(byte[] bytes) {
    int bytesLength = bytes.length;
    byte[] reversedBytes = new byte[bytesLength];
    for (int i = 0; i < bytesLength; i++) {
      reversedBytes[i] = bytes[bytesLength - i - 1];
    }
    return reversedBytes;
  }
}
