package com.secata.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Serializer for a List that safely reads/writes each value ordered to the the supplied stream.
 *
 * @param <ValueT> the type of each value
 */
public class SafeListStream<ValueT> extends SafeCollectionStream<ValueT, List<ValueT>> {

  private SafeListStream(
      Function<SafeDataInputStream, ValueT> reader,
      BiConsumer<ValueT, SafeDataOutputStream> writer,
      Function<Stream<ValueT>, List<ValueT>> collectionCreator) {
    super(reader, writer, collectionCreator);
  }

  /**
   * Creates a stream serializer for a list.
   *
   * @param reader reading a single element
   * @param writer writes a single element
   * @param <ValueT> the type of element to serialize with this list
   * @return the list loaded from this stream
   */
  public static <ValueT> SafeListStream<ValueT> create(
      Function<SafeDataInputStream, ValueT> reader,
      BiConsumer<ValueT, SafeDataOutputStream> writer) {
    return new SafeListStream<>(reader, writer, SafeListStream::construct);
  }

  private static <ValueT> List<ValueT> construct(Stream<ValueT> stream) {
    return stream.collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * Creates a stream serializer for a list consisting of nullable elements. The framework decides
   * how to support the presence. This is useful if reading a list of a fixed set of parties where
   * each one may be missing.
   *
   * @param reader reading a single element
   * @param writer writes a single element
   * @param <ValueT> the type of element to serialize with this list
   * @return the list loaded from this stream
   */
  public static <ValueT> SafeListStream<ValueT> createOptional(
      Function<SafeDataInputStream, ValueT> reader,
      BiConsumer<ValueT, SafeDataOutputStream> writer) {
    return create(
        (stream) -> stream.readOptional(reader),
        (value, stream) -> stream.writeOptional(writer, value));
  }
}
