package com.secata.stream;

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.util.NumericConversion;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

/**
 * Writer to an output stream, can write different types of basic types. Write in big-endian format.
 */
@SuppressWarnings("CPD-START")
public class BigEndianByteOutput implements ByteOutput {

  private final OutputStream stream;

  /**
   * Creates a new big-endian stream based on a raw output stream.
   *
   * @param stream the stream to wrap.
   */
  public BigEndianByteOutput(OutputStream stream) {
    this.stream = stream;
  }

  /**
   * Creates a BigEndianByteOutput, runs the serialization and returns the bytes.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static byte[] serialize(Consumer<BigEndianByteOutput> serializer) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    BigEndianByteOutput bigEndianByteOutput = new BigEndianByteOutput(stream);
    serializer.accept(bigEndianByteOutput);
    return stream.toByteArray();
  }

  @Override
  public void writeBytes(byte[] values) {
    ExceptionConverter.run(() -> stream.write(values), "Unable to write bytes");
  }

  @Override
  public void writeBoolean(boolean value) {
    if (value) {
      writeI8(1);
    } else {
      writeI8(0);
    }
  }

  @Override
  public void writeI8(byte value) {
    ExceptionConverter.run(() -> stream.write(value), "Unable to write byte");
  }

  @Override
  public void writeI8(int value) {
    if (value < -128 || value > 127) {
      throw new IllegalArgumentException("Value must be between -128 and 127, but was " + value);
    }
    ExceptionConverter.run(() -> stream.write(value), "Unable to write byte");
  }

  @Override
  public void writeI16(short value) {
    final byte[] bytes = NumericConversion.shortToBytes(value);
    ExceptionConverter.run(() -> stream.write(bytes, 0, 2), "Unable to write short");
  }

  @Override
  public void writeI16(int value) {
    if (value < -32768 || value > 32767) {
      throw new IllegalArgumentException(
          "Value must be between -32768 and 32767, but was " + value);
    }
    final byte[] bytes = NumericConversion.shortToBytes((short) value);
    ExceptionConverter.run(() -> stream.write(bytes, 0, 2), "Unable to write short");
  }

  @Override
  public void writeI32(int value) {
    final byte[] bytes = NumericConversion.intToBytes(value);
    ExceptionConverter.run(() -> stream.write(bytes), "Unable to write int");
  }

  @Override
  public void writeI64(long value) {
    final byte[] bytes = NumericConversion.longToBytes(value);
    ExceptionConverter.run(() -> stream.write(bytes), "Unable to write long");
  }

  @Override
  public void writeSignedBigInteger(BigInteger value, int noBytes) {
    var result = new byte[noBytes];
    NumericConversion.bigIntToBytes(value, noBytes, result, 0);
    writeBytes(result);
  }

  @Override
  public void writeU8(byte value) {
    writeI8(value);
  }

  @Override
  public void writeU8(int value) {
    if (value < 0 || value > 255) {
      throw new IllegalArgumentException("Value must be between 0 and 255, but was " + value);
    }
    ExceptionConverter.run(() -> stream.write(value), "Unable to write byte");
  }

  @Override
  public void writeU16(short value) {
    writeI16(value);
  }

  @Override
  public void writeU16(int value) {
    if (value < 0 || value > 65535) {
      throw new IllegalArgumentException("Value must be between 0 and 65535, but was " + value);
    }
    final byte[] bytes = NumericConversion.shortToBytes((short) value);
    ExceptionConverter.run(() -> stream.write(bytes, 0, 2), "Unable to write short");
  }

  @Override
  public void writeU32(int value) {
    writeI32(value);
  }

  @Override
  public void writeU64(long value) {
    writeI64(value);
  }

  @Override
  public void writeUnsignedBigInteger(BigInteger value, int noBytes) {
    if (value.signum() == -1) {
      throw new IllegalArgumentException(
          "Cannot write %s as unsigned: Value must not be negative".formatted(value));
    }
    var result = new byte[noBytes];
    NumericConversion.bigIntToBytesUnsigned(value, noBytes, result, 0);
    writeBytes(result);
  }

  @Override
  public void writeString(String value) {
    byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
    writeI32(bytes.length);
    writeBytes(bytes);
  }
}
