package com.secata.stream;

import java.util.function.Consumer;

/**
 * Defines something that can be written to a streaming interface as in {@link
 * SafeDataOutputStream}.
 */
@FunctionalInterface
public interface DataStreamSerializable extends Consumer<SafeDataOutputStream> {

  @Override
  default void accept(SafeDataOutputStream safeDataOutputStream) {
    write(safeDataOutputStream);
  }

  /**
   * Writes the object to a {@link SafeDataOutputStream}.
   *
   * @param stream the destination stream for this object
   */
  void write(SafeDataOutputStream stream);
}
