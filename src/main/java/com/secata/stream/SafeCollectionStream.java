package com.secata.stream;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Serializer for a collection type that safely reads/writes each value ordered to the the supplied
 * stream.
 *
 * @param <ValueT> the type of each value
 * @param <CollectionT> the type of collection for the value
 */
public abstract class SafeCollectionStream<ValueT, CollectionT extends Collection<ValueT>> {

  private final BiConsumer<ValueT, SafeDataOutputStream> writer;
  private final Function<Stream<ValueT>, CollectionT> collectionCreator;
  private final Function<SafeDataInputStream, ValueT> reader;

  /**
   * Constructs a new collection stream.
   *
   * @param reader the way to read from SafeDataInputStream
   * @param writer the way to write to SafeDataOutputStream
   * @param collectionCreator creates a new instance of
   */
  protected SafeCollectionStream(
      Function<SafeDataInputStream, ValueT> reader,
      BiConsumer<ValueT, SafeDataOutputStream> writer,
      Function<Stream<ValueT>, CollectionT> collectionCreator) {
    this.reader = reader;
    this.writer = writer;
    this.collectionCreator = collectionCreator;
  }

  /**
   * Converts a simple reader (from SafeDataOutputStream::read*) to use in collections.
   *
   * @param writer the writer
   * @param <ValueT> the primitive to serialize
   * @return the reader for the collection
   */
  public static <ValueT> BiConsumer<ValueT, SafeDataOutputStream> primitive(
      BiConsumer<SafeDataOutputStream, ValueT> writer) {
    return (value, stream) -> writer.accept(stream, value);
  }

  /**
   * Reads a list of objects from this stream.
   *
   * @param stream the conversion function called <code>count</code> times
   * @param count the number of elements to read
   * @return the list read.
   */
  public CollectionT readFixed(SafeDataInputStream stream, int count) {
    Stream<ValueT> valueStream = Stream.generate(() -> reader.apply(stream)).limit(count);
    return collectionCreator.apply(valueStream);
  }

  /**
   * Writes a list of objects to this stream, each element can serialize itself.
   *
   * @param stream destination for list
   * @param list element to serialize
   */
  public void writeFixed(SafeDataOutputStream stream, CollectionT list) {
    for (ValueT element : list) {
      writer.accept(element, stream);
    }
  }

  /**
   * Reads a list of objects from this stream, the first four bytes is the length of the list. Calls
   * {@link #readFixed(SafeDataInputStream, int)} afterwards.
   *
   * @param stream the stream to read from
   * @return the list read.
   */
  public CollectionT readDynamic(SafeDataInputStream stream) {
    int listLength = stream.readInt();
    return readFixed(stream, listLength);
  }

  /**
   * Writes a list of objects from this stream, the first four bytes is the length of the list.
   * Calls {@link #writeFixed(SafeDataOutputStream, Collection)} afterwards.
   *
   * @param stream the destination
   * @param list the elements to write
   */
  public void writeDynamic(SafeDataOutputStream stream, CollectionT list) {
    stream.writeInt(list.size());
    writeFixed(stream, list);
  }
}
