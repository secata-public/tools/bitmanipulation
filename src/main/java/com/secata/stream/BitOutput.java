package com.secata.stream;

import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.function.Consumer;

/**
 * Writer to an output stream. Able to write numbers with variable bit length. E.g. two integers
 * followed by five booleans and then a long is packed efficiently into the stream.
 */
public final class BitOutput {
  private final OutputStream stream;
  private int bitIndex = 0;
  private int lastByteToWrite = 0;
  private int noBytesWritten = 0;

  /**
   * Creates a new BitOutput based on a raw output stream.
   *
   * @param stream the stream to wrap.
   */
  public BitOutput(OutputStream stream) {
    this.stream = stream;
  }

  /**
   * Utility method for creating a BitOutput, run the serialization and return the bytes.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static byte[] serialize(Consumer<BitOutput> serializer) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    BitOutput bitOutput = new BitOutput(stream);
    serializer.accept(bitOutput);
    bitOutput.flushLastByte();
    return stream.toByteArray();
  }

  /**
   * Creates a BitOutput, runs the serialization and returns the bits as a compact bit array.
   *
   * @param serializer the serialization to happen
   * @return the serialized bits
   */
  public static CompactBitArray serializeBits(Consumer<BitOutput> serializer) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    BitOutput bitOutput = new BitOutput(stream);
    serializer.accept(bitOutput);
    int size = bitOutput.writtenBits();
    bitOutput.flushLastByte();
    return new CompactBitArray(stream.toByteArray(), size);
  }

  /**
   * Write a number of bytes from a byte array.
   *
   * @param bytes byte array containing the bytes to be written
   * @param offset starting offset of byte array
   * @param length number of bytes to write from offset
   * @return the BitOutput with the written bytes
   */
  public BitOutput writeBytes(byte[] bytes, int offset, int length) {
    if (bitIndex == 0) {
      ExceptionConverter.run(() -> stream.write(bytes, offset, length));
      noBytesWritten += length;
    } else {
      for (int i = 0; i < length; i++) {
        writeNumberBits(bytes[i + offset], 8);
      }
    }
    return this;
  }

  /**
   * Writes all the bytes from a byte array.
   *
   * @param bytes byte array containing the bytes to be written
   * @return the BitOutput with the written bytes
   */
  public BitOutput writeBytes(byte[] bytes) {
    return writeBytes(bytes, 0, bytes.length);
  }

  /**
   * Write the specified number of bits of an unsigned BigInteger to this builder.
   *
   * @param value the unsigned BigInteger to write
   * @param bitLength the number of bits of the BigInteger to write
   * @return the BitOutput with the written BigInteger
   */
  public BitOutput writeUnsignedBigInteger(BigInteger value, int bitLength) {
    isValueValid(value, bitLength);

    int fullBytes = bitLength >> 3;
    byte[] allBytes = new byte[fullBytes + 1];
    byte[] bytes = value.toByteArray();
    for (int i = 0; i < bytes.length; i++) {
      allBytes[i] = bytes[bytes.length - i - 1];
    }
    writeBytes(allBytes, 0, fullBytes);
    int remainingBytes = bitLength - fullBytes * 8;
    writeNumberBits(allBytes[fullBytes], remainingBytes);
    return this;
  }

  /**
   * Write the specified number of bits of an unsigned int to this builder. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle 32-bit unsigned integers in java.
   *
   * @param value the unsigned int to write
   * @param bitLength the number of bits of the int to write
   * @return the BitOutput with the written int
   */
  public BitOutput writeUnsignedInt(int value, int bitLength) {
    if (bitLength > 32) {
      throw new IllegalArgumentException(
          ">32 bit numbers must be written using BitOutput#writeUnsignedBigInteger");
    }
    if (bitLength == 32) {
      return writeSignedInt(value, 32);
    } else {
      return writeUnsignedBigInteger(BigInteger.valueOf(value), bitLength);
    }
  }

  /**
   * Write the specified number of bits of an unsigned long to this builder. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle 64-bit unsigned longs in java.
   *
   * @param value the unsigned long to write
   * @param bitLength the number of bits of the long to write
   * @return the BitOutput with the written long
   */
  public BitOutput writeUnsignedLong(long value, int bitLength) {
    if (bitLength > 64) {
      throw new IllegalArgumentException(
          ">64 bit numbers must be written using BitOutput#writeUnsignedBigInteger");
    }
    if (bitLength == 64) {
      return writeSignedLong(value, 64);
    } else {
      return writeUnsignedBigInteger(BigInteger.valueOf(value), bitLength);
    }
  }

  /**
   * Write the specified number of bits of a signed BigInteger to this builder.
   *
   * @param value the signed BigInteger to write
   * @param bitLength the number of bits of the BigInteger to write
   * @return the BitOutput with the written BigInteger
   */
  public BitOutput writeSignedBigInteger(BigInteger value, int bitLength) {
    isSignedValueValid(value, bitLength);
    if (value.signum() == -1) {
      BigInteger max = BigInteger.ONE.shiftLeft(bitLength);
      return writeUnsignedBigInteger(value.add(max), bitLength);
    } else {
      return writeUnsignedBigInteger(value, bitLength);
    }
  }

  /**
   * Write the specified number of bits of a signed int to this builder.
   *
   * @param value the signed int to write
   * @param bitLength the number of bits of the int to write
   * @return the BitOutput with the written int
   */
  public BitOutput writeSignedInt(int value, int bitLength) {
    if (bitLength > 32) {
      throw new IllegalArgumentException(
          ">32 bit numbers must be written using BitOutput#writeSignedBigInteger");
    }
    return writeSignedBigInteger(BigInteger.valueOf(value), bitLength);
  }

  /**
   * Write the specified number of bits of a signed long to this builder.
   *
   * @param value the signed long to write
   * @param bitLength the number of bits of the long to write
   * @return the BitOutput with the written long
   */
  public BitOutput writeSignedLong(long value, int bitLength) {
    if (bitLength > 64) {
      throw new IllegalArgumentException(
          ">64 bit numbers must be written using BitOutput#writeSignedBigInteger");
    }
    return writeSignedBigInteger(BigInteger.valueOf(value), bitLength);
  }

  private static void isValueValid(BigInteger value, int bitLength) {
    BigInteger max = BigInteger.ONE.shiftLeft(bitLength).subtract(BigInteger.ONE);
    if (value.signum() == -1 || value.compareTo(max) > 0) {
      throw new IllegalArgumentException(
          value + " cannot be represented as an unsigned " + bitLength + " bit number");
    }
  }

  private static void isSignedValueValid(BigInteger value, int bitLength) {
    BigInteger max = BigInteger.ONE.shiftLeft(bitLength - 1).subtract(BigInteger.ONE);
    BigInteger min = BigInteger.ZERO.subtract(BigInteger.ONE.shiftLeft(bitLength - 1));
    if (value.compareTo(max) > 0 || value.compareTo(min) < 0) {
      throw new IllegalArgumentException(
          value + " cannot be represented as a signed " + bitLength + " bit number");
    }
  }

  /**
   * Writes a number one bit at a time.
   *
   * @param value the number to be written
   * @param bitLength the number of bits to write
   */
  private void writeNumberBits(int value, int bitLength) {
    for (int i = 0; i < bitLength; i++) {
      boolean isBitSet = (value & (1 << i)) != 0;
      writeBoolean(isBitSet);
    }
  }

  /**
   * Write the specified boolean to this builder.
   *
   * @param bool the boolean to write
   * @return the BitOutput with the written boolean
   */
  public BitOutput writeBoolean(boolean bool) {
    if (bool) {
      lastByteToWrite = lastByteToWrite | (1 << bitIndex);
    }
    bitIndex++;
    if (bitIndex == 8) {
      bitIndex = 0;
      ExceptionConverter.run(() -> stream.write(lastByteToWrite));
      noBytesWritten++;
      lastByteToWrite = 0;
    }
    return this;
  }

  /** Writes the last byte to the stream even if all bits have not been written yet. */
  public void flushLastByte() {
    if (bitIndex != 0) {
      ExceptionConverter.run(() -> stream.write(lastByteToWrite));
      bitIndex = 0;
      noBytesWritten++;
    }
  }

  /**
   * Returns the number of written bits.
   *
   * @return the number of written bits
   */
  public int writtenBits() {
    return noBytesWritten * 8 + bitIndex;
  }
}
