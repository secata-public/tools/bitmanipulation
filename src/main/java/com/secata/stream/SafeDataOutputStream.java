package com.secata.stream;

import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Writer to an output stream, can write different types of basic types. Similar to
 * DataOutputStream, but with wrapped exceptions.
 */
public final class SafeDataOutputStream {

  private final DataOutputStream stream;

  /**
   * Creates a new safe stream based on a raw output stream.
   *
   * @param stream the stream to wrap.
   */
  public SafeDataOutputStream(OutputStream stream) {
    this.stream = new DataOutputStream(stream);
  }

  /**
   * Returns the number of bytes written to this data output stream so far.
   *
   * @return the number of bytes written to this data output stream so far.
   * @see DataOutputStream#size()
   */
  public int size() {
    return stream.size();
  }

  /**
   * Utility method for creating a SafeDataOutputStream, run the serialization and return the bytes.
   *
   * @param serializer the serialization to happen
   * @return the serialized bytes
   */
  public static byte[] serialize(Consumer<SafeDataOutputStream> serializer) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    SafeDataOutputStream safeDataOutputStream = new SafeDataOutputStream(stream);
    serializer.accept(safeDataOutputStream);
    return stream.toByteArray();
  }

  /**
   * Writes a long.
   *
   * @param l the long to write to the stream
   */
  public void writeLong(long l) {
    ExceptionConverter.run(() -> stream.writeLong(l), "Unable to write long");
  }

  /**
   * Writes an int.
   *
   * @param i the int to write to the stream
   */
  public void writeInt(int i) {
    ExceptionConverter.run(() -> stream.writeInt(i), "Unable to write int");
  }

  /**
   * Writes a short.
   *
   * @param i the short to write to the stream
   */
  public void writeShort(int i) {
    ExceptionConverter.run(() -> stream.writeShort(i), "Unable to write short");
  }

  /**
   * Writes a short as two bytes (i.e. only the two least significant bytes are used).
   *
   * @param i the short to write to the stream
   */
  public void writeShort(short i) {
    ExceptionConverter.run(() -> stream.writeShort(i), "Unable to write short");
  }

  /**
   * Writes a byte.
   *
   * @param b the byte to write to the stream
   */
  public void writeByte(int b) {
    ExceptionConverter.run(() -> stream.writeByte(b), "Unable to write byte");
  }

  /**
   * Writes a byte array.
   *
   * @param bytes the bytes to write
   */
  public void write(byte[] bytes) {
    ExceptionConverter.run(() -> stream.write(bytes), "Unable to write bytes");
  }

  /**
   * Writes a boolean value.
   *
   * @param b the boolean to write to the stream
   */
  public void writeBoolean(boolean b) {
    ExceptionConverter.run(() -> stream.writeBoolean(b), "Unable to write boolean");
  }

  /**
   * Writes an enum.
   *
   * @param anEnum the enum to write to the stream
   * @param <T> the type of enum
   */
  @SuppressWarnings("EnumOrdinal")
  public <T extends Enum<T>> void writeEnum(T anEnum) {
    writeByte(anEnum.ordinal());
  }

  /**
   * Writes a byte array prepended with the length.
   *
   * @param bytes the bytes to write
   */
  public void writeDynamicBytes(byte[] bytes) {
    writeInt(bytes.length);
    write(bytes);
  }

  /**
   * Writes a string to the stream.
   *
   * @param string the string to write.
   */
  public void writeString(String string) {
    byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
    writeDynamicBytes(bytes);
  }

  /**
   * Writes a uuid to the stream.
   *
   * @param value the uuid to write.
   */
  public void writeUuid(UUID value) {
    writeLong(value.getMostSignificantBits());
    writeLong(value.getLeastSignificantBits());
  }

  /**
   * Write a nullable value to the stream.
   *
   * @param writer writing the actual value
   * @param value the optional value to write
   * @param <ValueT> the type of the value
   */
  public <ValueT> void writeOptional(
      BiConsumer<ValueT, SafeDataOutputStream> writer, ValueT value) {
    boolean isPresent = value != null;
    writeBoolean(isPresent);
    if (isPresent) {
      writer.accept(value, this);
    }
  }
}
