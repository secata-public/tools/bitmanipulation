package com.secata.stream;

import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.function.Function;

/**
 * Reader of an input stream, can read different types of basic types. Similar to DataInputStream,
 * but with wrapped exceptions.
 */
public final class SafeDataInputStream {

  private final DataInputStream stream;

  /**
   * Reads a value from a byte array.
   *
   * @param serializer reader of the value from a stream
   * @param bytes the byte array which will be wrapped in a stream
   * @param <T> the type to read
   * @return the read object from the bytes with type T
   */
  public static <T> T deserialize(Function<SafeDataInputStream, T> serializer, byte[] bytes) {
    return serializer.apply(createFromBytes(bytes));
  }

  /**
   * Creates a stream from the bytes.
   *
   * @param bytes the bytes to wrap
   * @return the stream on the bytes
   */
  public static SafeDataInputStream createFromBytes(byte[] bytes) {
    return new SafeDataInputStream(new ByteArrayInputStream(bytes));
  }

  /**
   * Creates a new stream based on an input stream.
   *
   * @param stream the input stream to wrap
   */
  public SafeDataInputStream(InputStream stream) {
    this.stream = new DataInputStream(stream);
  }

  /**
   * Reads all the supplied bytes with the supplied creator.
   *
   * @param bytes the bytes to read
   * @param creator creates state from bytes
   * @param <T> type of state to create
   * @return state created
   * @throws IllegalStateException if the creator does not read every byte
   */
  public static <T> T readFully(byte[] bytes, Function<SafeDataInputStream, T> creator) {
    ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
    T result = creator.apply(new SafeDataInputStream(stream));
    int availableBytes = stream.available();
    if (availableBytes != 0) {
      throw new IllegalStateException(
          "Entire content of stream was not read. Remaining bytes: " + availableBytes);
    }
    return result;
  }

  /**
   * Reads a long.
   *
   * @return the read long from the stream
   */
  public long readLong() {
    return ExceptionConverter.call(stream::readLong, "Unable to read long");
  }

  /**
   * Reads an int.
   *
   * @return the read int from the stream
   */
  public int readInt() {
    return ExceptionConverter.call(stream::readInt, "Unable to read int");
  }

  /**
   * Reads an unsigned short.
   *
   * @return the read unsigned short from the stream
   */
  public int readUnsignedShort() {
    return ExceptionConverter.call(stream::readUnsignedShort, "Unable to read short");
  }

  /**
   * Reads a short.
   *
   * @return the read short from the stream
   */
  public short readShort() {
    return ExceptionConverter.call(stream::readShort, "Unable to read short");
  }

  /**
   * Reads an unsigned byte.
   *
   * @return the read unsigned byte from the stream
   */
  public int readUnsignedByte() {
    return ExceptionConverter.call(stream::readUnsignedByte, "Unable to read unsigned byte");
  }

  /**
   * Reads a signed byte.
   *
   * @return the read signed byte from the stream
   */
  public byte readSignedByte() {
    return ExceptionConverter.call(stream::readByte, "Unable to read signed byte");
  }

  /**
   * Reads a boolean.
   *
   * @return the read boolean from the stream
   */
  public boolean readBoolean() {
    return ExceptionConverter.call(stream::readBoolean, "Unable to read boolean");
  }

  /**
   * Reads an array of bytes.
   *
   * @param numberOfBytes the number of bytes
   * @return the read bytes
   */
  public byte[] readBytes(int numberOfBytes) {
    byte[] bytes =
        ExceptionConverter.call(() -> stream.readNBytes(numberOfBytes), "Unable to read bytes");
    if (bytes.length != numberOfBytes) {
      throw new IllegalStateException("Unable to read requested number of bytes");
    }
    return bytes;
  }

  /**
   * Reads all bytes to the end of the stream.
   *
   * @return the rest of the stream as bytes
   */
  public byte[] readAllBytes() {
    return ExceptionConverter.call(stream::readAllBytes, "Unable to read all bytes");
  }

  /**
   * Skip a number of bytes from this stream.
   *
   * @param numberOfBytes the number of bytes to skip
   * @throws IllegalStateException if unable to skip the requested number of bytes
   */
  public void skipBytes(int numberOfBytes) {
    int skipped =
        ExceptionConverter.call(() -> stream.skipBytes(numberOfBytes), "Unable to skip bytes");
    if (skipped != numberOfBytes) {
      throw new IllegalStateException("Unable to skip requested number of bytes");
    }
  }

  /**
   * Reads an enum value based on the type.
   *
   * @param values the values in the enum
   * @param <T> the trype of enum
   * @return the read value
   */
  public <T> T readEnum(T[] values) {
    return values[readUnsignedByte()];
  }

  /**
   * Reads a byte array with a prepended size as an integer.
   *
   * @return the byte array
   */
  public byte[] readDynamicBytes() {
    return readBytes(readInt());
  }

  /**
   * Reads a string from this stream.
   *
   * @return the read string
   */
  public String readString() {
    byte[] bytes = readDynamicBytes();
    return new String(bytes, StandardCharsets.UTF_8);
  }

  /**
   * Reads a uuid from the stream.
   *
   * @return the uuid from the stream
   */
  public UUID readUuid() {
    return new UUID(readLong(), readLong());
  }

  /**
   * Read a nullable value from the stream.
   *
   * @param reader reading the actual element
   * @param <ValueT> the type of the value to read
   * @return the read value or null if not present
   */
  public <ValueT> ValueT readOptional(Function<SafeDataInputStream, ValueT> reader) {
    boolean isPresent = readBoolean();
    if (isPresent) {
      return reader.apply(this);
    } else {
      return null;
    }
  }
}
