package com.secata.stream;

import java.math.BigInteger;

/** Common interface of an endian-aware writer to an output stream. */
public interface ByteOutput {

  /**
   * Writes a byte array.
   *
   * @param values the byte array to write to the stream
   */
  void writeBytes(byte[] values);

  /**
   * Writes a boolean.
   *
   * @param value the boolean to write to the stream
   */
  void writeBoolean(boolean value);

  /**
   * Writes a signed byte.
   *
   * @param value the byte to write to the stream
   */
  void writeI8(byte value);

  /**
   * Writes a signed byte from an int.
   *
   * @param value the int to write as a byte to the stream. Must be between -128 and 127
   */
  void writeI8(int value);

  /**
   * Writes a signed short.
   *
   * @param value the short to write to the stream
   */
  void writeI16(short value);

  /**
   * Writes a signed short from an int.
   *
   * @param value the int to write as a short to the stream. Must be between -32768 and 32767
   */
  void writeI16(int value);

  /**
   * Writes a signed int.
   *
   * @param value the int to write to the stream
   */
  void writeI32(int value);

  /**
   * Writes a signed long.
   *
   * @param value the long to write to the stream
   */
  void writeI64(long value);

  /**
   * Writes a signed BigInteger.
   *
   * @param value the BigInteger to write to the stream
   * @param noBytes the number of bytes to write
   */
  void writeSignedBigInteger(BigInteger value, int noBytes);

  /**
   * Writes an unsigned byte.
   *
   * @param value the byte to write to the stream
   */
  void writeU8(byte value);

  /**
   * Writes an unsigned byte from an int.
   *
   * @param value the int to write as a byte to the stream. Must be between 0 and 255
   */
  void writeU8(int value);

  /**
   * Writes an unsigned short.
   *
   * @param value the short to write to the stream
   */
  void writeU16(short value);

  /**
   * Writes an unsigned short from an int.
   *
   * @param value the int to write as a short to the stream. Must be between 0 and 65535
   */
  void writeU16(int value);

  /**
   * Writes an unsigned int.
   *
   * @param value the int to write to the stream
   */
  void writeU32(int value);

  /**
   * Writes an unsigned long.
   *
   * @param value the long to write to the stream
   */
  void writeU64(long value);

  /**
   * Writes an unsigned BigInteger.
   *
   * @param value the BigInteger to write to the stream
   * @param noBytes the number of bytes to write
   */
  void writeUnsignedBigInteger(BigInteger value, int noBytes);

  /**
   * Writes a string.
   *
   * @param value the string to write to the stream
   */
  void writeString(String value);
}
