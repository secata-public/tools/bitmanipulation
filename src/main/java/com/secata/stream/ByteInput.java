package com.secata.stream;

import java.math.BigInteger;

/** Common interface of an endian-aware reader for an input stream. */
public interface ByteInput {

  /**
   * Reads an array of bytes.
   *
   * @param noBytes the number of bytes
   * @return the read bytes
   */
  byte[] readBytes(int noBytes);

  /**
   * Reads a single byte from the stream as a boolean.
   *
   * @return false if the read byte is 0 and true otherwise
   */
  boolean readBoolean();

  /**
   * Reads a signed 8-bit number from the stream.
   *
   * @return the read number as a java byte
   */
  byte readI8();

  /**
   * Reads a signed 16-bit number from the stream.
   *
   * @return the read number as a java short
   */
  short readI16();

  /**
   * Reads a signed 32-bit number from the stream.
   *
   * @return the read number as a java int
   */
  int readI32();

  /**
   * Reads a signed 64-bit number from the stream.
   *
   * @return the read number as a java long
   */
  long readI64();

  /**
   * Reads a signed BigInteger.
   *
   * @param noBytes the number of bytes to read
   * @return the read BigInteger from the stream
   */
  BigInteger readSignedBigInteger(int noBytes);

  /**
   * Reads an unsigned 8-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java byte
   */
  byte readU8();

  /**
   * Reads an unsigned 16-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java short
   */
  short readU16();

  /**
   * Reads an unsigned 32-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java int
   */
  int readU32();

  /**
   * Reads an unsigned 64-bit number from the stream. See <a
   * href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">this</a> for
   * how to handle unsigned numbers.
   *
   * @return the read number as a java long
   */
  long readU64();

  /**
   * Reads an unsigned BigInteger.
   *
   * @param noBytes the number of bytes to read
   * @return the read BigInteger from the stream
   */
  BigInteger readUnsignedBigInteger(int noBytes);

  /**
   * Reads a string.
   *
   * @return the read string
   */
  String readString();

  /**
   * Reads all remaining bytes to the end of the stream.
   *
   * @return the rest of the stream as bytes
   */
  byte[] readRemaining();

  /**
   * Skip a number of bytes from this stream.
   *
   * @param numberOfBytes the number of bytes to skip
   * @throws IllegalStateException if unable to skip the requested number of bytes
   */
  void skipBytes(int numberOfBytes);
}
