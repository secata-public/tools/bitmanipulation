package com.secata.stream;

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.util.ByteHelper;
import com.secata.util.ShareConversion;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

/**
 * Reader of an input stream, can read different types of basic types. Reads in little-endian
 * format.
 */
public class LittleEndianByteInput implements ByteInput {
  private final InputStream stream;

  /**
   * Creates a new little-endian stream based on an input stream.
   *
   * @param stream the stream to wrap.
   */
  public LittleEndianByteInput(InputStream stream) {
    this.stream = stream;
  }

  @Override
  public byte[] readBytes(int noBytes) {
    return readBytesWithError(noBytes, "Unable to read bytes");
  }

  @Override
  public boolean readBoolean() {
    var value = readI8();
    return value != 0;
  }

  @Override
  public byte readI8() {
    int value = ExceptionConverter.call(stream::read, "Unable to read byte");
    if (value == -1) {
      throw new RuntimeException("Unable to read byte");
    }
    return (byte) value;
  }

  @Override
  public short readI16() {
    byte[] bytes = readBytesWithError(2, "Unable to read short");
    return ShareConversion.shortFromBytes(bytes, 0);
  }

  @Override
  public int readI32() {
    byte[] bytes = readBytesWithError(4, "Unable to read int");
    return ShareConversion.intFromBytes(bytes, 0);
  }

  @Override
  public long readI64() {
    byte[] bytes = readBytesWithError(8, "Unable to read long");
    return ShareConversion.longFromBytes(bytes, 0);
  }

  @Override
  public BigInteger readSignedBigInteger(int noBytes) {
    byte[] bytes =
        readBytesWithError(noBytes, "Unable to read big integer with " + noBytes + " bytes");
    return new BigInteger(ByteHelper.reverse(bytes));
  }

  @Override
  public byte readU8() {
    return readI8();
  }

  @Override
  public short readU16() {
    return readI16();
  }

  @Override
  public int readU32() {
    return readI32();
  }

  @Override
  public long readU64() {
    return readI64();
  }

  @Override
  public BigInteger readUnsignedBigInteger(int noBytes) {
    byte[] bytes =
        readBytesWithError(noBytes, "Unable to read big integer with " + noBytes + " bytes");
    return new BigInteger(1, ByteHelper.reverse(bytes));
  }

  @Override
  public String readString() {
    int length = readI32();
    byte[] bytes = readBytesWithError(length, "Unable to read string");
    return new String(bytes, StandardCharsets.UTF_8);
  }

  @Override
  public byte[] readRemaining() {
    return ExceptionConverter.call(stream::readAllBytes, "Unable to read remaining bytes");
  }

  @Override
  public void skipBytes(int numberOfBytes) {
    long skipped =
        ExceptionConverter.call(() -> stream.skip(numberOfBytes), "Unable to skip bytes");
    if (skipped != numberOfBytes) {
      throw new IllegalStateException("Unable to skip requested number of bytes");
    }
  }

  private byte[] readBytesWithError(int noBytes, String errorMessage) {
    if (noBytes == 0) {
      return new byte[0];
    }
    byte[] bytes = ExceptionConverter.call(() -> stream.readNBytes(noBytes), errorMessage);
    if (bytes.length != noBytes) {
      throw new RuntimeException(errorMessage);
    }
    return bytes;
  }
}
