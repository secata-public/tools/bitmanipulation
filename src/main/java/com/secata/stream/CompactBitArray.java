package com.secata.stream;

/** A bit array with the bits packed compactly in a byte array. */
@SuppressWarnings("ArrayRecordComponent")
public record CompactBitArray(byte[] data, int size) {

  /**
   * Create a new compact bit array.
   *
   * @param data the content of the bit array stored compactly in a byte array
   * @param size the number of bits in the bit array
   */
  public CompactBitArray {
    if (size > data.length * 8 || size <= (data.length - 1) * 8) {
      throw new IllegalArgumentException("Size doesn't match length of the data");
    }
  }
}
