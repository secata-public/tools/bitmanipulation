package com.secata.stream;

import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;

/**
 * Reader of an input stream. Able to read numbers with variable bit length. E.g. two integers
 * followed by five booleans and then a long is read from the stream perceived as a consecutive bit
 * array, the first 8 bytes is allocated to the two integers, the next byte has five bits allocated
 * to booleans, the next 3 bits, plus the next 8 bytes is the long.
 */
public final class BitInput {
  private final InputStream stream;
  private int bitIndex = 0;
  private int lastByteRead = 0;

  private BitInput(InputStream stream) {
    this.stream = stream;
  }

  /**
   * Create a new BitInput that reads the stream given.
   *
   * @param stream the stream to be read
   * @return the new BitInput
   */
  public static BitInput create(InputStream stream) {
    return new BitInput(stream);
  }

  /**
   * Create a new BitInput that reads the bytes given.
   *
   * @param bytes the bytes to be read
   * @return the new BitInput
   */
  public static BitInput create(byte[] bytes) {
    return new BitInput(new ByteArrayInputStream(bytes));
  }

  /**
   * Return a byte array of the specified length read from the next bytes.
   *
   * @param byteLength the number of bytes to read
   * @return the read bytes
   */
  public byte[] readBytes(int byteLength) {
    if (this.bitIndex == 0) {
      return readBytesBytewise(byteLength);
    } else {
      return readBytesBitwise(byteLength);
    }
  }

  /**
   * Reads the next 8 * byteLength bits one bit at a time and returns them as a byte array with
   * length byteLength.
   *
   * @param byteLength the number of bytes to read
   * @return the read bytes
   */
  private byte[] readBytesBitwise(int byteLength) {
    byte[] bytes = new byte[byteLength];
    for (int i = 0; i < byteLength; i++) {
      bytes[i] = (byte) this.readUnsignedInt(8);
    }
    return bytes;
  }

  /**
   * Reads the next byteLength bytes from the stream and returns them as a byte array. Assumes that
   * bitIndex is 0 such that the read bytes matches the next bytes of the BitInput stream.
   *
   * @param byteLength the number of bytes to read
   * @return the read bytes
   */
  private byte[] readBytesBytewise(int byteLength) {
    byte[] bytes = new byte[byteLength];
    int noBytesRead = ExceptionConverter.call(() -> stream.read(bytes, 0, byteLength));
    if (noBytesRead != byteLength) {
      throw new RuntimeException("Reached end of stream");
    } else {
      return bytes;
    }
  }

  /**
   * Read an unsigned BigInteger of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @return the BigInteger created from the read bits
   */
  public BigInteger readUnsignedBigInteger(int bitLength) {
    BigInteger sum = BigInteger.ZERO;
    for (int i = 0; i < bitLength; i++) {
      if (this.readBoolean()) {
        sum = sum.add(BigInteger.ONE.shiftLeft(i));
      }
    }
    return sum;
  }

  /**
   * Read an unsigned int of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @return the int created from the read bits
   */
  public int readUnsignedInt(int bitLength) {
    if (bitLength > 32) {
      throw new IllegalArgumentException(
          ">32 bit numbers must be read using BitInput#readUnsignedBigInteger");
    }
    return readUnsignedBigInteger(bitLength).intValue();
  }

  /**
   * Read an unsigned long of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @return the long created from the read bits
   */
  public long readUnsignedLong(int bitLength) {
    if (bitLength > 64) {
      throw new IllegalArgumentException(
          ">64 bit numbers must be read using BitInput#readUnsignedBigInteger");
    }
    return readUnsignedBigInteger(bitLength).longValue();
  }

  /**
   * Read a signed BigInteger of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @return the BigInteger created from the read bits
   */
  public BigInteger readSignedBigInteger(int bitLength) {
    BigInteger unsigned = readUnsignedBigInteger(bitLength);
    if (unsigned.testBit(bitLength - 1)) {
      BigInteger max = BigInteger.ONE.shiftLeft(bitLength);
      return unsigned.subtract(max);
    } else {
      return unsigned;
    }
  }

  /**
   * Read a signed int of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @return the int created from the read bits
   */
  public int readSignedInt(int bitLength) {
    if (bitLength > 32) {
      throw new IllegalArgumentException(
          ">32 bit numbers must be read using BitInput#readSignedBigInteger");
    }
    return readSignedBigInteger(bitLength).intValue();
  }

  /**
   * Read a signed long of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @return the long created from the read bits
   */
  public long readSignedLong(int bitLength) {
    if (bitLength > 64) {
      throw new IllegalArgumentException(
          ">64 bit numbers must be read using BitInput#readSignedBigInteger");
    }
    return readSignedBigInteger(bitLength).longValue();
  }

  /**
   * Read the next boolean.
   *
   * @return the read boolean
   */
  public boolean readBoolean() {
    if (bitIndex == 0) {
      lastByteRead = ExceptionConverter.call(stream::read);
      if (lastByteRead == -1) {
        throw new RuntimeException("Reached end of stream");
      }
    }
    boolean isBitSet = (lastByteRead & (1 << this.bitIndex)) > 0;
    bitIndex++;
    if (bitIndex == 8) {
      bitIndex = 0;
    }
    return isBitSet;
  }
}
