package com.secata.stream;

import com.secata.tools.immutable.FixedList;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Serializer for a fixed List that safely reads/writes each value ordered to the supplied stream.
 *
 * @param <ValueT> the type of each value
 */
public class SafeFixedListStream<ValueT> extends SafeCollectionStream<ValueT, FixedList<ValueT>> {

  private SafeFixedListStream(
      Function<SafeDataInputStream, ValueT> reader,
      BiConsumer<ValueT, SafeDataOutputStream> writer,
      Function<Stream<ValueT>, FixedList<ValueT>> collectionCreator) {
    super(reader, writer, collectionCreator);
  }

  /**
   * Creates a stream serializer for a list.
   *
   * @param reader reads a single element
   * @param writer writes a single element
   * @param <ValueT> the type of element to serialize with this list
   * @return the list loaded from this stream
   */
  public static <ValueT> SafeFixedListStream<ValueT> create(
      Function<SafeDataInputStream, ValueT> reader,
      BiConsumer<ValueT, SafeDataOutputStream> writer) {
    return new SafeFixedListStream<>(reader, writer, FixedList::create);
  }
}
